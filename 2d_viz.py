import sys, os, json, traceback
import errno, tempfile, requests
from os.path import dirname, basename, abspath, join as joinpath, isfile, splitext
import bpy, math
if any('blender29' in p for p in bpy.__path__) or any('29' in p for p in bpy.__path__):
    d = "/usr/local/blender29/2.92/python/lib/python3.7/site-packages"
    if d not in sys.path:
        sys.path.insert(0, d)
if any('blender2.93' in p for p in bpy.__path__) or any('2.93' in p for p in bpy.__path__):
    d = "/usr/local/blender2.93/2.93/python/lib/python3.9/site-packages"
    if d not in sys.path:
        sys.path.insert(0, d)
BASE_DIR = dirname(abspath(__file__))
import json
import numpy as np
import cv2
from PIL import Image
import PIL
if any('blender27' in p for p in bpy.__path__) or any('2.7' in p for p in bpy.__path__):
    bpy.context.user_preferences.addons['cycles'].preferences.compute_device_type='CUDA'
    for a in bpy.context.user_preferences.addons['cycles'].preferences.devices:
        a.use = True
    bpy.context.scene.cycles.device = 'GPU'
else:
    import _cycles
    avail_devices = _cycles.available_devices('CUDA')
    print(avail_devices)
    prop = bpy.context.preferences.addons['cycles'].preferences
    prop.get_devices(prop.compute_device_type)
    prop.compute_device_type = 'CUDA'
    for device in prop.devices:
        print(device, device.use)
        if device.type == 'CUDA':
            print('device: ', device)
            device.use = True
from bpy_extras.image_utils import load_image
import logging
print(sys.path)
#import IPython
#IPython.embed()
DEBUG_LEVEL = logging.getLevelName(os.environ.get('DEBUG_LEVEL', 'INFO'))

def to_rgb(c):                                                                                                                                                
    if c < 0.0031308:
        srgb = 0.0 if c < 0.0 else c * 12.92
    else:
        srgb = 1.055 * math.pow(c, 1.0 / 2.4) - 0.055

    return max(min(int(srgb * 255 + 0.5), 255), 0)

def to_srgb(c):
    c = (float(c) - 0.5)/255.0
    print(c)
    if c < 0.04044990748269014:
        rgb = 0.0 if c < 0.0 else c/12.92
    else:
        rgb = math.pow((0.055 + (c/1.055)), 2.4)
    return max(min(rgb, 1.0), 0.0)


def get_logger(name, level = None, formatter = None):
    logger = logging.getLogger(name)
    if not logger.handlers:
        h = logging.StreamHandler(sys.stderr)
        h.setFormatter(logging.Formatter(formatter or "%(asctime)s: name-%(name)s: func-%(funcName)s[%(lineno)s]: %(levelname)s:  %(message)s"))
        logger.addHandler(h)
    logger.setLevel(level or DEBUG_LEVEL)
    logger.propagate = False
    return logger
                             
logger = get_logger(__name__)

def get_texture_node(main_node, input_name):
    logger.info("Getting Texture node for %s - %s", main_node.name, input_name)
    try:
        i = main_node.inputs[input_name]
    except KeyError:
        raise KeyError("No input named: {} in node: {}, maybe wrong type of node?".format(input_name, main_node.name))
    if not i.links:
        return
    l = i.links[0]
    fn = l.from_node
    if fn.type == 'TEX_IMAGE':
        yield fn
    else:
        for inpm in fn.inputs.keys():
            try:
                for node in get_texture_node(fn, inpm):
                    yield node
            except StopIteration:
                return
            except Exception as e:
                logger.error("Error in getting node: {}".format(e))

def change_texture_principled(material_name, material_image = None, specular_image = None, bump_map = None, colour = None, roughness_factor = 0.8, transmission = 0.0, metallic = 0.0):
    logger.info("Changing material texture for %s", material_name)
    # Get node tree
    try:
        node_tree = bpy.data.materials[material_name].node_tree
    except KeyError:
        raise KeyError("Unknown material name called: {}".format(material_name))
    # Get principled node
    try:
        principled_node = node_tree.nodes["Principled BSDF"]
    except KeyError:
        raise KeyError("Unknown node of type Principled BSDF in material: {}".format(material_name))

    if material_image: 
        logger.info("Material Image as: %s", material_image)
        mimg = load_image(material_image)
        if mimg:
            for img_node in get_texture_node(principled_node, "Base Color"):
                img_node.image = None
                img_node.image = mimg
            logger.info("Updated node, complete: %s")
        if specular_image:
            spimg = load_image(material_image)
            if spimg:
                for img_node in get_texture_node(principled_node, "Specular"):
                    img_node.image = None
                    img_node.image = spimg

        if bump_map:
            try:
                bump_node = node_tree.nodes["Bump"]
            except KeyError:
                pass
            else:
                for img_node in get_texture_node(bump_node, "Height"):
                    img_node.image = None
                    img_node.image = mimg
                bimg = load_image(bump_map)
                if bimg:
                    for img_node in get_texture_node(bump_node, "Normal"):
                        img_node.image = None
                        img_node.image = bimg
    elif colour:
        logger.info("Colour is being set as: %s", colour)
        l1 = principled_node.inputs["Base Color"].links[0]
        l2 = principled_node.inputs["Normal"].links[0]
        l3 = principled_node.inputs["Specular"].links[0]
        node_tree.links.remove(l1); node_tree.links.remove(l2); node_tree.links.remove(l3);
        principled_node.inputs["Base Color"].default_value = tuple(map(to_srgb, (colour.split(",") if isinstance(colour, (str,)) else colour ) + [255.0]))
        logger.info("Removed Links")

    try:
        principled_node.inputs["Clearcoat"].default_value = max(0, (1 - roughness_factor*2))
        principled_node.inputs["Roughness"].default_value = roughness_factor
    except KeyError as e:
        raise KeyError("Cannot apply roughness on node: %s, wrong type of node perhaps?".format(principled_node.name))
    if transmission:
        if len(principled_node.inputs["Transmission"].links):
            l5 = principled_node.inputs["Transmission"].links[0]
            node_tree.links.remove(l5);
        try:
            principled_node.inputs["Transmission"].default_value = transmission
        except KeyError as e:
            raise KeyError("Cannot apply transmission on node: %s, wrong type of node perhaps?".format(principled_node.name))
    if metallic:
        if len(principled_node.inputs["Metallic"].links):
            l6 = principled_node.inputs["Metallic"].links[0]
            node_tree.links.remove(l6);
        try:
            principled_node.inputs["Metallic"].default_value = 0.9
        except KeyError as e:
            raise KeyError("Cannot apply metallic on node: %s, wrong type of node perhaps?".format(principled_node.name))
    else:
        principled_node.inputs["Metallic"].default_value = 0
    logger.info("Changed principled node complete"),
    return principled_node

def squarify(image_path, default_size = 2048, tile = True, rotate = None):
    image = cv2.imread(image_path)
    if image is None:
        return None
    now_shape = image.shape
    if not tile:
        mn = min(now_shape[:2])
        mn2 = mn//2
        centre = tuple(s//2 for s in now_shape[:2])
        cropped = image[max(0, centre[0]-mn2):centre[0]+mn2, max(0, centre[1]-mn2):centre[1]+mn2,:]
    else:
        mn = max(now_shape[:2])
        if now_shape[0] > now_shape[1]:
            cropped = np.concatenate((image, image), axis = 1)
            rotate = False
        elif now_shape[0] < now_shape[1]:
            cropped = np.concatenate((image, image), axis = 0)
            rotate = True
        else:
            cropped = image
            rotate = False
    if rotate:
        cropped = cv2.rotate(cropped, cv2.ROTATE_90_CLOCKWISE)
    s = default_size or get_closest_base_2(mn)
    resized = cv2.resize(cropped, (s, s))
    logger.info("Resized from {} to {}".format(now_shape, (s,s)))
    cv2.imwrite(image_path, resized)
    return image_path


def choose_camera(camera_id):
    cam = None
    try:
        cam = bpy.data.objects[camera_id]
    except KeyError:
        try:
            cam = bpy.data.objects["{}".format(camera_id)]
        except KeyError:
            for obj in bpy.data.objects:
                if obj.type == 'CAMERA':
                    cam = obj
                    break
    if cam:
        bpy.data.scenes['Scene'].camera = cam
    return cam

def mkdir_p(path):
    """ 'mkdir -p' in Python """
    try:
        os.makedirs(path)
        #print "path:%s",path
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def make_list(inp, mapper = None):
    """
    Function to convert a single object into a list
    """
    if not isinstance(inp, (list, tuple)):
        inp = [inp]
    if hasattr(mapper, '__call__'):
        return map(mapper, inp)
    return list(inp)

class FileSizeExceeded(Exception):
    pass

def download_file(url, directory, filepath = None, max_chunks = 102400000, chunk_size = 1024):
    # NOTE max_chunks is 1MB
    # NOTE the stream=True parameter
    if isfile(url):
        return url
    _, ext = splitext(url)
    mkdir_p(directory)
    if filepath:
        if isfile(joinpath(directory, filepath + ext)):
            logger.info("Found existing file: %s", joinpath(directory, filepath + ext))
            return joinpath(directory, filepath + ext)
    if filepath:
        string_file = open(joinpath(directory, filepath + ext), 'wb')
    else:
        string_file = tempfile.NamedTemporaryFile(suffix = ext, dir = directory, delete = False)
    r = requests.get(url, stream=True)
    if r.status_code >= 400:
        return None
    num_chunks = 0
    try:
        for chunk in r.iter_content(chunk_size = chunk_size): 
            if chunk: # filter out keep-alive new chunks
                string_file.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
                num_chunks += 1
            if num_chunks >= max_chunks:
                raise FileSizeExceeded("File size exceeded {} Kb for URL: {}".format(max_chunks, url))
    except Exception as e:
        logger.error(e)
        return None
    else:
        return string_file.name
    finally:
        string_file.close()

def do_apply_material(config, render = False):
    base_dir = config.get('output_dir') or BASE_DIR
    mkdir_p(base_dir)
    surfaces = config.get("surface_ids")
    room_id = config.get("room_id")
    design_ids = config.get("design_ids")
    if isinstance(design_ids, str):
        with open(design_ids, "r") as fp:
            design_ids = json.load(fp)
    skip_surfaces = config.get('skip_surfaces', {})
    re_render = config.get('re_render', False)
    if not surfaces and room_id and design_id:
        logger.error("Could not find surfaces, rooms and designs and camera in the config")
        sys.exit(1)
    camera_ids = config.get("camera_ids")
    holed_done = {}
    offset = config.get('design_offset', 0)
    for design_id, finish_type, _ in make_list(design_ids[offset:]):
        design_url = '{}/{}.{}'.format(config.get('design_path'), design_id, config.get('design_ext', 'jpg'))
        logger.info("Downloading image: %s", design_url)
        material_image = download_file(design_url, 'tmp', filepath = design_id, max_chunks = 20480000)
        if not material_image:
            continue
        logger.info("Material Image: %s", material_image)
        rotate = config.get('rotate', False);
        squarify(material_image, 1024, rotate = False) #config.get('rotate', False))
        roughness_factor = ROUGHNESS_MAP.get(finish_type.lower(), 0.5)
        transmission = (finish_type.lower() in ["transluscent", "translucent"]) and config.get("transmission", 1.0)
        metallic = (finish_type.lower() in ["metallic"]) and config.get("metallic", 1.0)
        for camera_id in make_list(camera_ids):
            if camera_id not in holed_done:
                holed_done[camera_id] = False
            choose_camera(camera_id)
            file_ext = EXT_MAP.get(config.get('file_extension'), 'PNG')
            scene = bpy.context.scene
            scene.cycles.device = 'GPU'
            scene.cycles.samples = 128
            scene.render.image_settings.file_format = file_ext
            scene.render.image_settings.color_mode = 'RGBA'
            scene.render.film_transparent = True
            scene.cycles.denoiser = 'OPTIX'
            scene.cycles.use_denoising = True
            scene.cycles.use_adaptive_sampling = True
            scene.cycles.adaptive_threshold = 0.01
            scene.cycles.max_bounces = 4
            scene.cycles.transmission_bounces = 3
            scene.cycles.transparent_max_bounces = 3
            scene.render.use_border = False
            scene.render.use_crop_to_border = False
            scene.render.tile_x = 265
            scene.render.tile_y = 265
            scene.render.use_persistent_data = True
            scene.render.resolution_x = int(config.get("rendering_resolution", "1280x657").lower().split("x")[0])
            scene.render.resolution_y = int(config.get("rendering_resolution", "1280x657").lower().split("x")[1])
            scene.render.resolution_percentage = 100
            scene = bpy.data.scenes["Scene"]
            scene.cycles.device = 'GPU'
            scene.cycles.samples = 128
            scene.render.image_settings.file_format = file_ext
            scene.render.image_settings.color_mode = 'RGBA'
            if 'node_tree' in scene.keys():
                scene.node_tree.nodes["Composite"].use_alpha = True
                scene.node_tree.nodes["Viewer"].use_alpha = True
            scene.render.film_transparent = True
            scene.cycles.denoiser = 'OPTIX'
            scene.cycles.use_denoising = True
            scene.cycles.use_adaptive_sampling = True
            scene.cycles.adaptive_threshold = 0.01
            scene.cycles.max_bounces = 4
            scene.cycles.transmission_bounces = 3
            scene.cycles.transparent_max_bounces = 3
            scene.render.use_border = False
            scene.render.use_crop_to_border = False
            scene.render.tile_x = 265
            scene.render.tile_y = 265
            scene.render.use_persistent_data = True
            scene.render.resolution_x = int(config.get("rendering_resolution", "1280x657").lower().split("x")[0])
            scene.render.resolution_y = int(config.get("rendering_resolution", "1280x657").lower().split("x")[1])
            scene.render.resolution_percentage = 100
            for surface_id in surfaces:
                for s in surfaces:
                    for ob in bpy.data.collections[s].all_objects.values():
                        logger.info("Object name: %s", ob.name)
                        ob.cycles.is_holdout = True
                        if 'cycles_visibility' in ob:
                            ob['cycles_visibility']['camera'] = True
                col = bpy.data.collections[surface_id]
                logger.info("col_name: %s", surface_id)
                col.hide_render = False
                if surface_id != config.get('holed_out'):
                    ff = '{}_{}_{}_{}_{}'.format(room_id, camera_id, surface_id, design_id.replace(' ','_'), finish_type.replace('/','_'))
                    scene.render.filepath = joinpath(base_dir, ff)
                    url_path = joinpath('https://general-iamdave-mumbai.s3.ap-south-1.amazonaws.com/merino_digitalfolder/temp_renders', ff)
                    url_path1 = joinpath('https://general-iamdave.s3.us-west-2.amazonaws.com/octopus_products/renders', ff)
                    if isfile(scene.render.filepath+'.'+config.get('file_extension')) and not isfile(scene.render.filepath+'.webp'):
                        image = Image.open(scene.render.filepath + '.' + config.get('file_extension'))
                        image = image.convert('RGBA')
                        image.save(scene.render.filepath + '.webp', 'webp')
                    if not re_render and (isfile(scene.render.filepath+'.'+config.get('file_extension')) or isfile(scene.render.filepath+'.webp')):
                        continue
                    if isfile(scene.render.filepath+'.'+config.get('file_extension')):
                        continue
                    #df = download_file(url_path + '.webp', base_dir, ff)
                    #if df and isfile(df):
                    #    continue
                    #df = download_file(url_path1 + '.webp', base_dir, ff)
                    #if df and isfile(df):
                    #    continue
                    if render and (surface_id not in skip_surfaces or skip_surfaces[surface_id] == design_id):
                        for ob in col.all_objects.values():
                            logger.info("Object name for visibility True: %s", ob.name)
                            ob.cycles.is_holdout = False
                            material_name = ob.active_material.name
                            change_texture_principled(
                                material_name, 
                                material_image = material_image, 
                                roughness_factor = roughness_factor,
                                transmission = transmission,
                                metallic = metallic
                            )
                        logger.info("Changed all the textures")
                        scene.render.filepath = joinpath(base_dir, ff)
                        bpy.ops.render.render(write_still = True, scene = "Scene")
                        image = Image.open(scene.render.filepath + '.' + config.get('file_extension'))
                        image = image.convert('RGBA')
                        image.save(scene.render.filepath + '.webp', 'webp')
                elif render and not holed_done.get(camera_id):
                    scene = bpy.data.scenes["Scene"]
                    scene.render.filepath = joinpath(base_dir,'{}_{}_holed_out'.format(room_id, camera_id))
                    if isfile(scene.render.filepath+'.'+config.get('file_extension')) and not isfile(scene.render.filepath+'.webp'):
                        image = Image.open(scene.render.filepath + '.' + config.get('file_extension'))
                        image = image.convert('RGBA')
                        image.save(scene.render.filepath + '.webp', 'webp')
                    if not re_render and (isfile(scene.render.filepath+'.'+config.get('file_extension')) or isfile(scene.render.filepath+'.webp')):
                        continue
                    if isfile(scene.render.filepath+'.'+config.get('file_extension')):
                        continue
                    for ob in col.all_objects.values():
                        logger.info("Object name: %s", ob.name)
                        ob.cycles.is_holdout = False
                    bpy.ops.render.render(write_still = True, scene = "Scene")
                    image = Image.open(scene.render.filepath + '.' + config.get('file_extension'))
                    image = image.convert('RGBA')
                    image.save(scene.render.filepath + '.webp', 'webp')
                    holed_done[camera_id] = True

def get_sys_args():
    nf = sys.argv.index('--')
    return sys.argv[nf+1]

EXT_MAP = {
    'jpg': 'JPEG',
    'png': 'PNG',
    'jpeg': 'JPEG'
}
ROUGHNESS_MAP = {
    "standard": 0.5,
    "satin": 0.3,
    "matt": 0.8,
    "matte": 0.8,
    "suede": 0.8,
    "gloss": 0.2,
    "mirror": 0.05,
    "brush": 0.9,
    "metallic": 0.1,
    "textured": 0.8
}

CONFIG = {
    "room_id": "kitchen",
    "design_ids": [
      ["1000", "Suede"],
      ["1000", "Gloss"],
      ["1001", "Suede"],
      ["1002", "Gloss"]
    ],
    "design_path": "https://general-iamdave.s3.us-west-2.amazonaws.com/merinodigital",
    "design_ext": "jpg",
    "camera_ids": ["cabinets_view"],
    "holed_out": "other_parts",
    "surface_ids": [
        "cladding",
        "counter_top",
        "kitchen_shelf_1",
        "kitchen_shelf_2",
        "other_parts"
    ]
}

if __name__ == "__main__":
    try:
        sa = get_sys_args()
    except (IndexError, ValueError):
        logger.error("Cound not find a config file: %s", " ".join(sys.argv))
        do_apply_material(CONFIG, True)
        #sys.exit(0)
    else:
        try:
            if not isfile(sa):
                logger.error("Cound not find a config file at path: %s", sa)
                sys.exit(0)
            else:
                json_file = sa
                json_name, ext = splitext(json_file)
                json_name = basename(json_name)
                with open(sa, 'r') as fp:
                    config = json.load(fp)
                    do_apply_material(config, True)
        except Exception as e:
            logger.error(e)
            tr = traceback.format_exc() 
            logger.error(tr)
            sys.exit(1)
else:
    do_apply_material(CONFIG, True)

        

