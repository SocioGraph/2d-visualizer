import sys, os, json, traceback
import errno, tempfile, requests
from os.path import dirname, basename, abspath, join as joinpath, isfile, splitext
import bpy, math
BASE_DIR = dirname(abspath(__file__))
import json
import _cycles
avail_devices = _cycles.available_devices('CUDA')
print(avail_devices)
prop = bpy.context.preferences.addons['cycles'].preferences
prop.get_devices(prop.compute_device_type)
prop.compute_device_type = 'CUDA'
for device in prop.devices:
    print(device, device.use)
    if device.type == 'CUDA':
        print('device: ', device)
        device.use = True
from bpy_extras.image_utils import load_image
import logging
print(sys.path)
#import IPython
#IPython.embed()
DEBUG_LEVEL = logging.getLevelName(os.environ.get('DEBUG_LEVEL', 'INFO'))

def to_rgb(c):                                                                                                                                                
    if c < 0.0031308:
        srgb = 0.0 if c < 0.0 else c * 12.92
    else:
        srgb = 1.055 * math.pow(c, 1.0 / 2.4) - 0.055

    return max(min(int(srgb * 255 + 0.5), 255), 0)

def to_srgb(c):
    c = (float(c) - 0.5)/255.0
    print(c)
    if c < 0.04044990748269014:
        rgb = 0.0 if c < 0.0 else c/12.92
    else:
        rgb = math.pow((0.055 + (c/1.055)), 2.4)
    return max(min(rgb, 1.0), 0.0)


def get_logger(name, level = None, formatter = None):
    logger = logging.getLogger(name)
    if not logger.handlers:
        h = logging.StreamHandler(sys.stderr)
        h.setFormatter(logging.Formatter(formatter or "%(asctime)s: name-%(name)s: func-%(funcName)s[%(lineno)s]: %(levelname)s:  %(message)s"))
        logger.addHandler(h)
    logger.setLevel(level or DEBUG_LEVEL)
    logger.propagate = False
    return logger
                             
logger = get_logger(__name__)

def get_texture_node(main_node, input_name):
    logger.info("Getting Texture node for %s - %s", main_node.name, input_name)
    try:
        i = main_node.inputs[input_name]
    except KeyError:
        raise KeyError("No input named: {} in node: {}, maybe wrong type of node?".format(input_name, main_node.name))
    if not i.links:
        return
    l = i.links[0]
    fn = l.from_node
    if fn.type == 'TEX_IMAGE':
        yield fn
    else:
        for inpm in fn.inputs.keys():
            try:
                for node in get_texture_node(fn, inpm):
                    yield node
            except StopIteration:
                return
            except Exception as e:
                logger.error("Error in getting node: {}".format(e))

def change_texture_principled(material_name, material_image = None, specular_image = None, bump_map = None, colour = None, roughness_factor = 0.8, transmission = 0.0, metallic = 0.0):
    logger.info("Changing material texture for %s", material_name)
    # Get node tree
    try:
        node_tree = bpy.data.materials[material_name].node_tree
    except KeyError:
        raise KeyError("Unknown material name called: {}".format(material_name))
    # Get principled node
    try:
        principled_node = node_tree.nodes["Principled BSDF"]
    except KeyError:
        raise KeyError("Unknown node of type Principled BSDF in material: {}".format(material_name))

    if material_image: 
        logger.info("Material Image as: %s", material_image)
        mimg = load_image(material_image)
        if mimg:
            for img_node in get_texture_node(principled_node, "Base Color"):
                img_node.image = None
                img_node.image = mimg
            logger.info("Updated node, complete: %s")
        if specular_image:
            spimg = load_image(material_image)
            if spimg:
                for img_node in get_texture_node(principled_node, "Specular"):
                    img_node.image = None
                    img_node.image = spimg

        if bump_map:
            try:
                bump_node = node_tree.nodes["Bump"]
            except KeyError:
                pass
            else:
                for img_node in get_texture_node(bump_node, "Height"):
                    img_node.image = None
                    img_node.image = mimg
                bimg = load_image(bump_map)
                if bimg:
                    for img_node in get_texture_node(bump_node, "Normal"):
                        img_node.image = None
                        img_node.image = bimg
    elif colour:
        logger.info("Colour is being set as: %s", colour)
        l1 = principled_node.inputs["Base Color"].links[0]
        l2 = principled_node.inputs["Normal"].links[0]
        l3 = principled_node.inputs["Specular"].links[0]
        node_tree.links.remove(l1); node_tree.links.remove(l2); node_tree.links.remove(l3);
        principled_node.inputs["Base Color"].default_value = tuple(map(to_srgb, (colour.split(",") if isinstance(colour, (str,)) else colour ) + [255.0]))
        logger.info("Removed Links")

    try:
        principled_node.inputs["Clearcoat"].default_value = max(0, (1 - roughness_factor*2))
        principled_node.inputs["Roughness"].default_value = roughness_factor
    except KeyError as e:
        raise KeyError("Cannot apply roughness on node: %s, wrong type of node perhaps?".format(principled_node.name))
    if transmission:
        if len(principled_node.inputs["Transmission"].links):
            l5 = principled_node.inputs["Transmission"].links[0]
            node_tree.links.remove(l5);
        try:
            principled_node.inputs["Transmission"].default_value = transmission
        except KeyError as e:
            raise KeyError("Cannot apply transmission on node: %s, wrong type of node perhaps?".format(principled_node.name))
    if metallic:
        if len(principled_node.inputs["Metallic"].links):
            l6 = principled_node.inputs["Metallic"].links[0]
            node_tree.links.remove(l6);
        try:
            principled_node.inputs["Metallic"].default_value = 0.9
        except KeyError as e:
            raise KeyError("Cannot apply metallic on node: %s, wrong type of node perhaps?".format(principled_node.name))
    else:
        principled_node.inputs["Metallic"].default_value = 0
    logger.info("Changed principled node complete"),
    return principled_node

def squarify(image_path, default_size = 2048, tile = True, rotate = None):
    image = cv2.imread(image_path)
    if image is None:
        return None
    now_shape = image.shape
    if not tile:
        mn = min(now_shape[:2])
        mn2 = mn//2
        centre = tuple(s//2 for s in now_shape[:2])
        cropped = image[max(0, centre[0]-mn2):centre[0]+mn2, max(0, centre[1]-mn2):centre[1]+mn2,:]
    else:
        mn = max(now_shape[:2])
        if now_shape[0] > now_shape[1]:
            cropped = np.concatenate((image, image), axis = 1)
            rotate = False
        elif now_shape[0] < now_shape[1]:
            cropped = np.concatenate((image, image), axis = 0)
            rotate = True
        else:
            cropped = image
            rotate = False
    if rotate:
        cropped = cv2.rotate(cropped, cv2.ROTATE_90_CLOCKWISE)
    s = default_size or get_closest_base_2(mn)
    resized = cv2.resize(cropped, (s, s))
    logger.info("Resized from {} to {}".format(now_shape, (s,s)))
    cv2.imwrite(image_path, resized)
    return image_path


def choose_camera(camera_id):
    cam = None
    try:
        cam = bpy.data.objects[camera_id]
    except KeyError:
        try:
            cam = bpy.data.objects["{}".format(camera_id)]
        except KeyError:
            for obj in bpy.data.objects:
                if obj.type == 'CAMERA':
                    cam = obj
                    break
    if cam:
        bpy.data.scenes['Scene'].camera = cam
    return cam

def mkdir_p(path):
    """ 'mkdir -p' in Python """
    try:
        os.makedirs(path)
        #print "path:%s",path
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def make_list(inp, mapper = None):
    """
    Function to convert a single object into a list
    """
    if not isinstance(inp, (list, tuple)):
        inp = [inp]
    if hasattr(mapper, '__call__'):
        return map(mapper, inp)
    return list(inp)

class FileSizeExceeded(Exception):
    pass

def download_file(url, directory, filepath = None, max_chunks = 102400000, chunk_size = 1024):
    # NOTE max_chunks is 1MB
    # NOTE the stream=True parameter
    if isfile(url):
        return url
    _, ext = splitext(url)
    mkdir_p(directory)
    if filepath:
        if isfile(joinpath(directory, filepath + ext)):
            logger.info("Found existing file: %s", joinpath(directory, filepath + ext))
            return joinpath(directory, filepath + ext)
    if filepath:
        string_file = open(joinpath(directory, filepath + ext), 'wb')
    else:
        string_file = tempfile.NamedTemporaryFile(suffix = ext, dir = directory, delete = False)
    r = requests.get(url, stream=True)
    if r.status_code >= 400:
        return None
    num_chunks = 0
    try:
        for chunk in r.iter_content(chunk_size = chunk_size): 
            if chunk: # filter out keep-alive new chunks
                string_file.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
                num_chunks += 1
            if num_chunks >= max_chunks:
                raise FileSizeExceeded("File size exceeded {} Kb for URL: {}".format(max_chunks, url))
    except Exception as e:
        logger.error(e)
        return None
    else:
        return string_file.name
    finally:
        string_file.close()

def do_apply_material(config, render = False):
    base_dir = config.get('output_dir') or BASE_DIR
    mkdir_p(base_dir)
    surfaces = config.get("surface_ids")
    room_id = config.get("room_id")
    design_ids = config.get("design_ids")
    if isinstance(design_ids, str):
        with open(design_ids, "r") as fp:
            design_ids = json.load(fp)
    skip_surfaces = config.get('skip_surfaces', {})
    re_render = config.get('re_render', False)
    if not surfaces and room_id and design_id:
        logger.error("Could not find surfaces, rooms and designs and camera in the config")
        sys.exit(1)
    camera_ids = config.get("camera_ids")
    holed_done = {}
    offset = config.get('design_offset', 0)
    for design_id, finish_type, _ in make_list(design_ids[offset:]):
        design_url = '{}/{}.{}'.format(config.get('design_path'), design_id, config.get('design_ext', 'jpg'))
        logger.info("Downloading image: %s", design_url)
        material_image = download_file(design_url, joinpath(BASE_DIR,'tmp'), filepath = design_id, max_chunks = 20480000)
        if not material_image:
            logger.error("Material Image: %s NOT FOUND", design_url)
            continue
        logger.info("Material Image: %s", material_image)
        rotate = config.get('rotate', False);
        #squarify(material_image, 1024, rotate = False) #config.get('rotate', False))
        roughness_factor = ROUGHNESS_MAP.get(finish_type.lower(), 0.5)
        transmission = (finish_type.lower() in ["transluscent", "translucent"]) and config.get("transmission", 1.0)
        metallic = (finish_type.lower() in ["metallic"]) and config.get("metallic", 1.0)
        for camera_id in make_list(camera_ids):
            if camera_id not in holed_done:
                holed_done[camera_id] = False
            choose_camera(camera_id)
            file_ext = EXT_MAP.get(config.get('file_extension'), 'PNG')
            scene = bpy.context.scene
            scene.cycles.device = 'GPU'
            scene.cycles.samples = 128
            scene.render.image_settings.file_format = file_ext
            scene.render.image_settings.color_mode = 'RGBA'
            scene.render.film_transparent = True
            scene.cycles.denoiser = 'OPTIX'
            scene.cycles.use_denoising = True
            scene.cycles.use_adaptive_sampling = True
            scene.cycles.adaptive_threshold = 0.01
            scene.cycles.max_bounces = 4
            scene.cycles.transmission_bounces = 3
            scene.cycles.transparent_max_bounces = 3
            scene.render.use_border = False
            scene.render.use_crop_to_border = False
            scene.render.tile_x = 265
            scene.render.tile_y = 265
            scene.render.use_persistent_data = True
            scene.render.resolution_x = int(config.get("rendering_resolution", "1280x657").lower().split("x")[0])
            scene.render.resolution_y = int(config.get("rendering_resolution", "1280x657").lower().split("x")[1])
            scene.render.resolution_percentage = 100
            scene = bpy.data.scenes["Scene"]
            scene.cycles.device = 'GPU'
            scene.cycles.samples = 128
            scene.render.image_settings.file_format = file_ext
            scene.render.image_settings.color_mode = 'RGBA'
            if 'node_tree' in scene.keys():
                scene.node_tree.nodes["Composite"].use_alpha = True
                scene.node_tree.nodes["Viewer"].use_alpha = True
            scene.render.film_transparent = True
            scene.cycles.denoiser = 'OPTIX'
            scene.cycles.use_denoising = True
            scene.cycles.use_adaptive_sampling = True
            scene.cycles.adaptive_threshold = 0.01
            scene.cycles.max_bounces = 4
            scene.cycles.transmission_bounces = 3
            scene.cycles.transparent_max_bounces = 3
            scene.render.use_border = False
            scene.render.use_crop_to_border = False
            scene.render.tile_x = 265
            scene.render.tile_y = 265
            scene.render.use_persistent_data = True
            scene.render.resolution_x = int(config.get("rendering_resolution", "1280x657").lower().split("x")[0])
            scene.render.resolution_y = int(config.get("rendering_resolution", "1280x657").lower().split("x")[1])
            scene.render.resolution_percentage = 100
            for surface_id in surfaces:
                for s in surfaces:
                    for ob in bpy.data.collections[s].all_objects.values():
                        logger.info("Object name: %s", ob.name)
                        ob.cycles.is_holdout = True
                        if 'cycles_visibility' in ob:
                            ob['cycles_visibility']['camera'] = True
                col = bpy.data.collections[surface_id]
                logger.info("col_name: %s", surface_id)
                col.hide_render = False
                if surface_id != config.get('holed_out'):
                    if not rotate:
                        ff = '{}_{}_{}_{}_{}_horizontal'.format(room_id, camera_id, surface_id, design_id.replace(' ','_'), finish_type.replace('/','_'))
                    else:
                        ff = '{}_{}_{}_{}_{}'.format(room_id, camera_id, surface_id, design_id.replace(' ','_'), finish_type.replace('/','_'))
                    scene.render.filepath = joinpath(base_dir, ff)
                    if isfile(scene.render.filepath+'.'+config.get('file_extension')):
                        continue
                    if render and (surface_id not in skip_surfaces or skip_surfaces[surface_id] == design_id):
                        for ob in col.all_objects.values():
                            logger.info("Object name for visibility True: %s", ob.name)
                            ob.cycles.is_holdout = False
                            material_name = ob.active_material.name
                            change_texture_principled(
                                material_name, 
                                material_image = material_image, 
                                roughness_factor = roughness_factor,
                                transmission = transmission,
                                metallic = metallic
                            )
                        logger.info("Changed all the textures")
                        scene.render.filepath = joinpath(base_dir, ff)
                        bpy.ops.render.render(write_still = True, scene = "Scene")
                elif render and not holed_done.get(camera_id):
                    scene = bpy.data.scenes["Scene"]
                    scene.render.filepath = joinpath(base_dir,'{}_{}_holed_out'.format(room_id, camera_id))
                    for ob in col.all_objects.values():
                        logger.info("Object name: %s", ob.name)
                        ob.cycles.is_holdout = False
                    bpy.ops.render.render(write_still = True, scene = "Scene")
                    holed_done[camera_id] = True

def get_sys_args():
    nf = sys.argv.index('--')
    return sys.argv[nf+1]

EXT_MAP = {
    'jpg': 'JPEG',
    'png': 'PNG',
    'jpeg': 'JPEG'
}
ROUGHNESS_MAP = {
    "standard": 0.5,
    "satin": 0.3,
    "matt": 0.8,
    "matte": 0.8,
    "suede": 0.8,
    "gloss": 0.2,
    "mirror": 0.05,
    "brush": 0.9,
    "metallic": 0.1,
    "textured": 0.8
}

CONFIG = {
    "blender_config": "2d_viz_octopus.py",
    "blender_file": "assets/hospital_room_octopus.blend",
    "blender_version": "2.9",
    "camera_ids": [
        "master_view"
    ],
    "design_ext": "jpg",
    "design_ids": [
        [
            "177",
            "TEXTURED",
            1
        ],
        [
            "1000",
            "MATT",
            1
        ],
        [
            "1001",
            "MATT",
            1
        ],
        [
            "1002",
            "MATT",
            1
        ],
        [
            "1006",
            "MATT",
            1
        ],
        [
            "1011",
            "MATT",
            1
        ],
        [
            "1012",
            "MATT",
            1
        ],
        [
            "1013",
            "MATT",
            1
        ],
        [
            "1014",
            "MATT",
            1
        ],
        [
            "1016",
            "METALLIC",
            1
        ],
        [
            "1017",
            "METALLIC",
            1
        ],
        [
            "1018",
            "METALLIC",
            1
        ],
        [
            "1019",
            "METALLIC",
            1
        ],
        [
            "101C",
            "MATT",
            1
        ],
        [
            "1020",
            "TEXTURED",
            1
        ],
        [
            "1021",
            "TEXTURED",
            1
        ],
        [
            "1022",
            "METALLIC",
            1
        ],
        [
            "1023",
            "METALLIC",
            1
        ],
        [
            "1025",
            "MATT",
            1
        ],
        [
            "1026",
            "MATT",
            1
        ],
        [
            "1028",
            "MATT",
            1
        ],
        [
            "1029",
            "MATT",
            1
        ],
        [
            "1030",
            "MATT",
            1
        ],
        [
            "1032",
            "MATT",
            1
        ],
        [
            "1035",
            "MATT",
            1
        ],
        [
            "1039",
            "MATT",
            1
        ],
        [
            "1040",
            "MATT",
            1
        ],
        [
            "1042",
            "MATT",
            1
        ],
        [
            "1043",
            "MATT",
            1
        ],
        [
            "1044",
            "MATT",
            1
        ],
        [
            "1045",
            "MATT",
            1
        ],
        [
            "1049",
            "MATT",
            1
        ],
        [
            "1051",
            "MATT",
            1
        ],
        [
            "1053",
            "MATT",
            1
        ],
        [
            "1054",
            "MATT",
            1
        ],
        [
            "1055",
            "MATT",
            1
        ],
        [
            "1056",
            "MATT",
            1
        ],
        [
            "1058",
            "MATT",
            1
        ],
        [
            "1061",
            "MATT",
            1
        ],
        [
            "1067",
            "MATT",
            1
        ],
        [
            "1068",
            "MATT",
            1
        ],
        [
            "1071",
            "MATT",
            1
        ],
        [
            "1072",
            "MATT",
            1
        ],
        [
            "1075",
            "MATT",
            1
        ],
        [
            "1078",
            "MATT",
            1
        ],
        [
            "1082",
            "MATT",
            1
        ],
        [
            "1086",
            "MATT",
            1
        ],
        [
            "1087",
            "MATT",
            1
        ],
        [
            "1088",
            "MATT",
            1
        ],
        [
            "1091",
            "MATT",
            1
        ],
        [
            "1092",
            "MATT",
            1
        ],
        [
            "1093",
            "MATT",
            1
        ],
        [
            "1094",
            "MATT",
            1
        ],
        [
            "1095",
            "MATT",
            1
        ],
        [
            "1096",
            "MATT",
            1
        ],
        [
            "1097",
            "MATT",
            1
        ],
        [
            "1098",
            "MATT",
            1
        ],
        [
            "1099",
            "MATT",
            1
        ],
        [
            "11",
            "MIRROR",
            1
        ],
        [
            "1100",
            "MATT",
            1
        ],
        [
            "1101",
            "MATT",
            1
        ],
        [
            "1102",
            "MATT",
            1
        ],
        [
            "1103",
            "MATT",
            1
        ],
        [
            "1104",
            "MATT",
            1
        ],
        [
            "1105",
            "MATT",
            1
        ],
        [
            "1106",
            "MATT",
            1
        ],
        [
            "1107",
            "MATT",
            1
        ],
        [
            "1108",
            "MATT",
            1
        ],
        [
            "1109",
            "MATT",
            1
        ],
        [
            "1110",
            "MATT",
            1
        ],
        [
            "1111",
            "MATT",
            1
        ],
        [
            "1112",
            "MATT",
            1
        ],
        [
            "1113",
            "MATT",
            1
        ],
        [
            "1114",
            "MATT",
            1
        ],
        [
            "1116",
            "MATT",
            1
        ],
        [
            "1117",
            "MATT",
            1
        ],
        [
            "1118",
            "MATT",
            1
        ],
        [
            "1121",
            "MATT",
            1
        ],
        [
            "1122",
            "MATT",
            1
        ],
        [
            "1123",
            "MATT",
            1
        ],
        [
            "1124",
            "MATT",
            1
        ],
        [
            "1125",
            "MATT",
            1
        ],
        [
            "1126",
            "MATT",
            1
        ],
        [
            "1127",
            "MATT",
            1
        ],
        [
            "1129",
            "MATT",
            1
        ],
        [
            "1131",
            "MATT",
            1
        ],
        [
            "1132",
            "MATT",
            1
        ],
        [
            "1133",
            "MATT",
            1
        ],
        [
            "1134",
            "MATT",
            1
        ],
        [
            "1135",
            "MATT",
            1
        ],
        [
            "1136",
            "MATT",
            1
        ],
        [
            "1139",
            "MATT",
            1
        ],
        [
            "1140",
            "MATT",
            1
        ],
        [
            "1141",
            "MATT",
            1
        ],
        [
            "1142",
            "MATT",
            1
        ],
        [
            "1143",
            "MATT",
            1
        ],
        [
            "1144",
            "MATT",
            1
        ],
        [
            "1146",
            "MATT",
            1
        ],
        [
            "1147",
            "MATT",
            1
        ],
        [
            "1148",
            "MATT",
            1
        ],
        [
            "1149",
            "MATT",
            1
        ],
        [
            "1150",
            "MATT",
            1
        ],
        [
            "1151",
            "MATT",
            1
        ],
        [
            "1152",
            "MATT",
            1
        ],
        [
            "1153",
            "MATT",
            1
        ],
        [
            "1154",
            "MATT",
            1
        ],
        [
            "1155",
            "MATT",
            1
        ],
        [
            "1156",
            "MATT",
            1
        ],
        [
            "1157",
            "MATT",
            1
        ],
        [
            "1159",
            "MATT",
            1
        ],
        [
            "1160",
            "MATT",
            1
        ],
        [
            "1161",
            "MATT",
            1
        ],
        [
            "1162",
            "MATT",
            1
        ],
        [
            "1163",
            "MATT",
            1
        ],
        [
            "1164",
            "MATT",
            1
        ],
        [
            "1165",
            "MATT",
            1
        ],
        [
            "1166",
            "MATT",
            1
        ],
        [
            "1167",
            "MATT",
            1
        ],
        [
            "1169",
            "MATT",
            1
        ],
        [
            "1170",
            "MATT",
            1
        ],
        [
            "1176",
            "MATT",
            1
        ],
        [
            "1177",
            "MATT",
            1
        ],
        [
            "1178",
            "MATT",
            1
        ],
        [
            "1179",
            "MATT",
            1
        ],
        [
            "1180",
            "MATT",
            1
        ],
        [
            "1181",
            "MATT",
            1
        ],
        [
            "1182",
            "MATT",
            1
        ],
        [
            "1183",
            "MATT",
            1
        ],
        [
            "1184",
            "MATT",
            1
        ],
        [
            "1185",
            "MATT",
            1
        ],
        [
            "1186",
            "MATT",
            1
        ],
        [
            "1187",
            "MATT",
            1
        ],
        [
            "15",
            "BRUSH",
            1
        ],
        [
            "154",
            "MATT",
            1
        ],
        [
            "155",
            "MATT",
            1
        ],
        [
            "16",
            "BRUSH",
            1
        ],
        [
            "161",
            "MATT",
            1
        ],
        [
            "163",
            "MATT",
            1
        ],
        [
            "164",
            "MATT",
            1
        ],
        [
            "168",
            "MATT",
            1
        ],
        [
            "17",
            "SATIN",
            1
        ],
        [
            "170",
            "MATT",
            1
        ],
        [
            "171",
            "MATT",
            1
        ],
        [
            "174",
            "TEXTURED",
            1
        ],
        [
            "175",
            "TEXTURED",
            1
        ],
        [
            "179",
            "TEXTURED",
            1
        ],
        [
            "18",
            "MATT",
            1
        ],
        [
            "185",
            "MATT",
            1
        ],
        [
            "189",
            "TEXTURED",
            1
        ],
        [
            "18BR",
            "BRUSH",
            1
        ],
        [
            "19",
            "MATT",
            1
        ],
        [
            "190",
            "TEXTURED",
            1
        ],
        [
            "191",
            "MATT",
            1
        ],
        [
            "192",
            "MATT",
            1
        ],
        [
            "193",
            "TEXTURED",
            1
        ],
        [
            "194",
            "MATT",
            1
        ],
        [
            "195",
            "MATT",
            1
        ],
        [
            "197",
            "MATT",
            1
        ],
        [
            "1B",
            "MIRROR",
            1
        ],
        [
            "1BR",
            "BRUSH",
            1
        ],
        [
            "20",
            "MATT",
            1
        ],
        [
            "204",
            "MATT",
            1
        ],
        [
            "208",
            "MATT",
            1
        ],
        [
            "21",
            "MATT",
            1
        ],
        [
            "22",
            "MIRROR",
            1
        ],
        [
            "224",
            "MATT",
            1
        ],
        [
            "227",
            "MATT",
            1
        ],
        [
            "23",
            "BRUSH",
            1
        ],
        [
            "248",
            "MATT",
            1
        ],
        [
            "25",
            "MATT",
            1
        ],
        [
            "252",
            "METALLIC",
            1
        ],
        [
            "255",
            "MATT",
            1
        ],
        [
            "260",
            "MATT",
            1
        ],
        [
            "262",
            "MATT",
            1
        ],
        [
            "263",
            "MATT",
            1
        ],
        [
            "271",
            "METALLIC",
            1
        ],
        [
            "274",
            "TEXTURED",
            1
        ],
        [
            "288",
            "MATT",
            1
        ],
        [
            "289",
            "MATT",
            1
        ],
        [
            "290",
            "MATT",
            1
        ],
        [
            "293",
            "MATT",
            1
        ],
        [
            "294",
            "MATT",
            1
        ],
        [
            "2B",
            "MIRROR",
            1
        ],
        [
            "2BR",
            "BRUSH",
            1
        ],
        [
            "3",
            "MIRROR",
            1
        ],
        [
            "462",
            "MATT",
            1
        ],
        [
            "517",
            "METALLIC",
            1
        ],
        [
            "532",
            "MATT",
            1
        ],
        [
            "534",
            "MATT",
            1
        ],
        [
            "538",
            "MATT",
            1
        ],
        [
            "576",
            "METALLIC",
            1
        ],
        [
            "578",
            "METALLIC",
            1
        ],
        [
            "5BR",
            "BRUSH",
            1
        ],
        [
            "6",
            "MIRROR",
            1
        ],
        [
            "6BR",
            "BRUSH",
            1
        ],
        [
            "727",
            "METALLIC",
            1
        ],
        [
            "728",
            "METALLIC",
            1
        ],
        [
            "731",
            "METALLIC",
            1
        ],
        [
            "732",
            "METALLIC",
            1
        ],
        [
            "737",
            "MATT",
            1
        ],
        [
            "740",
            "METALLIC",
            1
        ],
        [
            "806",
            "SATIN",
            1
        ],
        [
            "81",
            "SATIN",
            1
        ],
        [
            "817",
            "COPPER",
            1
        ],
        [
            "818",
            "COPPER",
            1
        ],
        [
            "819",
            "COPPER",
            1
        ],
        [
            "82",
            "SATIN",
            1
        ],
        [
            "822",
            "COPPER",
            1
        ],
        [
            "837",
            "MIRROR",
            1
        ],
        [
            "838",
            "MATT",
            1
        ],
        [
            "839",
            "MATT",
            1
        ],
        [
            "84",
            "SATIN",
            1
        ],
        [
            "849",
            "COPPER",
            1
        ],
        [
            "853",
            "COPPER",
            1
        ],
        [
            "856",
            "COPPER",
            1
        ],
        [
            "857",
            "COPPER",
            1
        ],
        [
            "866",
            "COPPER",
            1
        ],
        [
            "903",
            "MATT",
            1
        ],
        [
            "905",
            "MATT",
            1
        ],
        [
            "906",
            "MATT",
            1
        ],
        [
            "911",
            "TEXTURED",
            1
        ],
        [
            "913",
            "MATT",
            1
        ],
        [
            "915",
            "MATT",
            1
        ],
        [
            "916",
            "MATT",
            1
        ],
        [
            "917",
            "MATT",
            1
        ],
        [
            "919",
            "TEXTURED",
            1
        ],
        [
            "920",
            "TEXTURED",
            1
        ],
        [
            "921",
            "TEXTURED",
            1
        ],
        [
            "927",
            "MATT",
            1
        ],
        [
            "935",
            "MATT",
            1
        ],
        [
            "936",
            "MATT",
            1
        ],
        [
            "943",
            "MATT",
            1
        ],
        [
            "944",
            "MATT",
            1
        ],
        [
            "951",
            "MATT",
            1
        ],
        [
            "957",
            "MATT",
            1
        ],
        [
            "959",
            "MATT",
            1
        ],
        [
            "961",
            "MATT",
            1
        ],
        [
            "962",
            "MATT",
            1
        ],
        [
            "964",
            "MATT",
            1
        ],
        [
            "966",
            "MATT",
            1
        ],
        [
            "968",
            "TEXTURED",
            1
        ],
        [
            "971",
            "GLOSS",
            1
        ],
        [
            "972",
            "MATT",
            1
        ],
        [
            "973",
            "MATT",
            1
        ],
        [
            "976",
            "MATT",
            1
        ],
        [
            "977",
            "MATT",
            1
        ],
        [
            "978",
            "MATT",
            1
        ],
        [
            "981",
            "MATT",
            1
        ],
        [
            "984",
            "MATT",
            1
        ],
        [
            "986",
            "MATT",
            1
        ],
        [
            "989",
            "MATT",
            1
        ],
        [
            "991",
            "MATT",
            1
        ],
        [
            "993",
            "MATT",
            1
        ],
        [
            "994",
            "MATT",
            1
        ],
        [
            "995",
            "MATT",
            1
        ],
        [
            "997",
            "MATT",
            1
        ],
        [
            "998",
            "MATT",
            1
        ],
        [
            "999",
            "MATT",
            1
        ],
        [
            "GL100",
            "GLOSS",
            1
        ],
        [
            "GL154",
            "GLOSS",
            1
        ],
        [
            "GL155",
            "GLOSS",
            1
        ],
        [
            "OCTOWEAVE A2W",
            "BEECH/CHERRY/MAPLE",
            1
        ],
        [
            "OCTOWEAVE B1W",
            "CHERRY/MAPLE",
            1
        ],
        [
            "OP8003",
            "HDF",
            1
        ],
        [
            "OP8009",
            "HDF",
            1
        ],
        [
            "OP8012",
            "HDF",
            1
        ],
        [
            "OP8013",
            "HDF/MAPLE",
            1
        ],
        [
            "OP8014",
            "HDF",
            1
        ],
        [
            "OP8017",
            "HDF/BEECH",
            1
        ],
        [
            "OP8019",
            "HDF/MAPLE",
            1
        ],
        [
            "OP8020",
            "HDF/MAPLE",
            1
        ],
        [
            "OP8021",
            "HDF/MAPLE",
            1
        ],
        [
            "OP8022",
            "HDF/MAPLE",
            1
        ],
        [
            "OP8023",
            "HDF/MAPLE",
            1
        ],
        [
            "OTM02",
            "MATT",
            1
        ],
        [
            "OTM03",
            "MATT",
            1
        ],
        [
            "OTM04",
            "MATT",
            1
        ],
        [
            "OTM05",
            "MATT",
            1
        ],
        [
            "OTM06",
            "MATT",
            1
        ],
        [
            "OTM07",
            "MATT",
            1
        ],
        [
            "OTM10",
            "MATT",
            1
        ],
        [
            "OTM11",
            "MATT",
            1
        ],
        [
            "OTM12",
            "MATT",
            1
        ],
        [
            "OTM13",
            "MATT",
            1
        ],
        [
            "OTM14",
            "MATT",
            1
        ],
        [
            "OTM15",
            "MATT",
            1
        ],
        [
            "OTM16",
            "MATT",
            1
        ],
        [
            "OTM17",
            "MATT",
            1
        ],
        [
            "OTM18",
            "MATT",
            1
        ],
        [
            "OTM19",
            "MATT",
            1
        ],
        [
            "OTM20",
            "MATT",
            1
        ],
        [
            "OTM21",
            "MATT",
            1
        ],
        [
            "OTM22",
            "MATT",
            1
        ],
        [
            "OTP21",
            "MATT",
            1
        ],
        [
            "OTP22",
            "MATT",
            1
        ],
        [
            "OTP23",
            "MATT",
            1
        ],
        [
            "OTP24",
            "MATT",
            1
        ],
        [
            "OTP25",
            "MATT",
            1
        ],
        [
            "OTP26",
            "MATT",
            1
        ],
        [
            "OTP27",
            "MATT",
            1
        ],
        [
            "OTP28",
            "MATT",
            1
        ],
        [
            "OTP29",
            "MATT",
            1
        ],
        [
            "OTP30",
            "MATT",
            1
        ],
        [
            "OTP31",
            "MATT",
            1
        ],
        [
            "OTP33",
            "MATT",
            1
        ],
        [
            "OTP34",
            "MATT",
            1
        ],
        [
            "OTP35",
            "MATT",
            1
        ],
        [
            "OTP36",
            "MATT",
            1
        ],
        [
            "OTP37",
            "MATT",
            1
        ],
        [
            "OTP38",
            "MATT",
            1
        ],
        [
            "OTP39",
            "MATT",
            1
        ],
        [
            "OTP40",
            "MATT",
            1
        ],
        [
            "OTP41",
            "MATT",
            1
        ],
        [
            "OTP42",
            "MATT",
            1
        ],
        [
            "OTP43",
            "MATT",
            1
        ],
        [
            "OTP44",
            "MATT",
            1
        ],
        [
            "OTP45",
            "MATT",
            1
        ],
        [
            "OTP46",
            "MATT",
            1
        ],
        [
            "OTP47",
            "MATT",
            1
        ],
        [
            "SMOKE MIRROR",
            "MIRROR",
            1
        ],
        [
            "Z102",
            "AFP",
            1
        ],
        [
            "Z104",
            "AFP",
            1
        ],
        [
            "Z105",
            "AFP",
            1
        ],
        [
            "Z106",
            "AFP",
            1
        ],
        [
            "Z108",
            "AFP",
            1
        ],
        [
            "Z109",
            "AFP",
            1
        ],
        [
            "86",
            "SATIN",
            1
        ],
        [
            "87",
            "SATIN",
            1
        ]
    ],
    "design_path": "https://dashboard.iamdave.ai/static/uploads/blender/tmp",
    "file_extension": "png",
    "holed_out": "other_parts",
    "rendering_resolution": "1280x657",
    "room_id": "hospital_room",
    "rotate": True,
    "surface_ids": [
        "other_parts",
        "doors",
        "wall_cladding",
        "cabinet",
        "cabinet_doors"
    ]
}

if __name__ == "__main__":
    try:
        sa = get_sys_args()
    except (IndexError, ValueError):
        logger.error("Cound not find a config file: %s", " ".join(sys.argv))
        do_apply_material(CONFIG, True)
        #sys.exit(0)
    else:
        try:
            if not isfile(sa):
                logger.error("Cound not find a config file at path: %s", sa)
                sys.exit(0)
            else:
                json_file = sa
                json_name, ext = splitext(json_file)
                json_name = basename(json_name)
                with open(sa, 'r') as fp:
                    config = json.load(fp)
                    do_apply_material(config, True)
        except Exception as e:
            logger.error(e)
            tr = traceback.format_exc() 
            logger.error(tr)
            sys.exit(1)
else:
    do_apply_material(CONFIG, True)

        


