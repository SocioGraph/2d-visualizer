



def get_greeting(self,engagement,*args,**kwargs):
    """
    This is a customer greeting function get called at the time of __init__ call and return greeting text.

    Input Args:
      - greet_text. 
        1. If it has {day_greeting} it will get replace with GM.GE etc. based on current time.
        2. If It has {customer_name} it will get replaced with user first_name, if they are in Facebook or Instagram or Twitter
      - customer_name (optional input)
        1. If present {customer_name} gets replaced with customer_name
      - bot_name (optional input)
        1. If present {bot_name} gets replaced with bot_name
     
    Example:
        greet= self.customer_greeting(engagement,greet_text = "Hi {customer_name}, {day_greeting} I am {bot_name} how can I help you.")
        
        Output:
        Hi Nikit, Good morning I am dave how can I help you.
    """

    cust = engagement.get("customer_profile",{})
    channel_id = cust.get("source",None)
    grt = hp.get_greeting()
    s = kwargs.get("greet_text",None)
    n = kwargs.get("customer_name", None)
  
    if s:
        if "{day_greeting}" in s:
            s = s.replace("{day_greeting}",grt)

        if "{bot_name}" in s:
            s= s.replace("{bot_name}","")

        if channel_id:
            if channel_id in ["instagram","facebook","twitter"]:
                if "{customer_name}" in s:
                    s= s.replace("{customer_name}",cust.get("first_name")) 
            s  = s.replace("{customer_name}","")
        elif n:
            s = s.replace("{customer_name}", n)
        else:
            s = s.replace("{customer_name}","")

    return s


def initial_attributes(self,engagement,*args,**kwargs):
    p = {}
    m= {}
    response  = ''
    k = kwargs.get("required_models",["product"])
    
    channel_id = engagement.get("customer_profile",{})
    channel_id = channel_id.get("source",None)
    split =  self.spliters(channel_id) if channel_id else self.spliters("test")

    #Base cases - comented for testing
    for i in k:
        m[i+'_model'] = base_model.Model(i, engagement.enterprise_id)
    

    return p,m,split

def spliters(self,channel_id):
    spliters_dict = {
        "whatsapp": "\n",
        "facebook": u"\u000A",
        "twitter": "\n",
        "test": "<br>",
        "instagram": "\n",
        "web-avatar": "<br>",
        "web-chatbot": "<br>"
    }
    return spliters_dict[channel_id]

def parse_selected_option(self, engagement, *args, **kwargs):
    n = None
    
    k = [("en_four","4")]

    for i,v in k:
        if engagement.get(i):
            n = v
            break
        
    if engagement.get("_number_"):
        n = engagement.get("_number_")

    if engagement.get("intent"):
        kwargs["number_picked"] = n
        return self.process_intent(engagement, *args, **kwargs)
    return


def get_missing_gl_val(self, engagement, *args, **kwargs):
    
    required = engagement.get("required")
    #check if there are required variables for the intent
    if required and len(required) > 0:
        # get first requirement
        i = required[0]
        engagement.logger.info("trying to get variable = {}".format(i))
        required_value = engagement.get("gl_"+i) if i else None
        engagement.logger.info("got required value = {}".format(required_value))
        if not required_value:
            engagement.logger.info("trying to run function == gl_{} bool == {}".format(i,callable(getattr(self, "get_"+ (i if i else ""), None))))
            # confirm if requirement is met or not
            f = getattr(self,"get_"+i)

            #check if function exists for required variable
            if not callable(getattr(self, "get_"+i, None)):
                engagement.logger.err("get_{} function not defined for intent = {}".format(i,i))
                return "unknown_customer_state"
            ret = f(engagement,*args,**kwargs)
            # if current requirement has sub requirements or is satisfied, returns null after adding to requirement list
            if not ret:
                # if new requirements added, call again to do those first
                return self.get_missing_gl_val(engagement,*args,**kwargs)
            #return system response name for current variable question with options list
            engagement.logger.info("returning system response as = "+ret)
            return ret
        else:
            #if requirement is already met, pop top and call again for next required var 
            self.pop_from_req(engagement)
            return self.get_missing_gl_val(engagement,*args,**kwargs)
    # no further required vars then return none and try do intent
    return None


def get_option_list_from_pivot(self,engagement, pivot_data):
    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(pivot_data.keys())
    options_str_list = ["{}. {}{}".format(k+1,v,split) for k,v in options]
    options_str = "".join(options_str_list)
    return options_str


def get_option_list_from_filter(self,engagement, filter_list, name_key):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_key) is list:
        options_list = []
        for k,v in options:
            vals = [v.get(name_keyy) for name_keyy in name_key]
            options_list.append(", ".join(vals))
        return options_list
    
    options_list = [v.get(name_key) for k,v in options]
    
    return options_list


def get_missing_gl_sub_req_val(self, engagement, *args, **kwargs):
    for i in engagement.get("sub_required",[]):
        if not engagement.get("gl"+i):
            return i
    return None


def add_to_required(self, engagement, var):
    hp.logger.info("adding {} to required".format(var))
    current_required = engagement.get("required")
    current_required.insert(0,var)
    engagement.save(current_required,"required")
    hp.logger.info("required ==  {} ".format(engagement.get("required")))

def add_variable_to_engagment(self, engagement,var,val):
    engagement.save(val,"gl_"+var)

    self.pop_from_req(engagement,var)
    # required.pop(var)
    # engagement.save(val,"gl_"+var)
    pass

def pop_from_req(self,engagement,var=None):
    req = engagement.get("required")
    hp.logger.info("trying to remove var from req == {}".format(var))
    if var and var in req:
        hp.logger.info("current required list == {}".format(req))
        hp.logger.info("trying to remove == {}".format(var))
        req.remove(var)
    elif not var:
        if req:
            req.pop(0)
    engagement.save(req,"required")


def set_intent(self, engagement, *args, **kwargs):
    logger.info("Set intent kwargs == {}".format(kwargs))
    required = hp.copy(kwargs.get("required",[])) if isinstance(kwargs.get("required",[]),list) else []
    recheck_required = kwargs.get("recheck_required")
    if recheck_required:
        for attr in recheck_required:
            if engagement.get("gl_{}".format(attr)):
                engagement.save(engagement.get("gl_{}".format(attr)),"gl_old_{}".format(attr))
                engagement.save(None,"gl_{}".format(attr))
    engagement.save(kwargs.get("failure_response","unknown_customer_state"),"gl_failure_response")
    intent = kwargs.get("intent")
    #previous_intent = engagement.get("intent" )#TODO check if its a diff intent ask for confirmation
    if intent:
        hp.logger.info("got required in if == {}".format(required))
        engagement.save(intent,"intent")
        engagement.save(required,"required")
    hp.logger.info("got intent == ".format(intent))
    hp.logger.info("got required == {}".format(required))

    return self.process_intent( engagement, *args, **kwargs)


def process_intent(self, engagement, *args,**kwargs):
    previous_intent = engagement.get("intent",None)
    #incase there is no intent anymore
    if not previous_intent:
        engagement.logger.info("No intent found")
    ret = self.get_missing_gl_val(engagement, *args, **kwargs)
    
    if ret:
        return ret
    previous_intent = engagement.get("intent",None)
    engagement.logger.info("previous intent --{}".format(previous_intent))
    #incase there is no intent anymore
    if not previous_intent:
        engagement.logger.info("No intent found")
        return "unknown_customer_state"
    f = getattr(self,"get_{}".format(engagement.get("intent")))
    
    #check if get intent function exists
    if not f:
        engagement.logger.err("get_{} function not defined for intent = {}".format(engagement.get("intent"),engagement.get("intent")))
        return engagement.get("gl_failure_response","unknown_customer_state")
    response = f(engagement,*args,**kwargs)
    if not response:
        return engagement.get("gl_failure_response","unknown_customer_state")
    engagement.save(None,"intent")
    engagement.save([],"required")
    if kwargs.get('is_unknown'):
        r = engagement.get('response') or ''
        r = "I'm not sure what I understand what you mean.<br>" + r
    return response 


#Formats your response with {splits} and adds options to data/whiteboard/placeholder according to channel
def structure_and_set_response(self, engagement, *args, **kwargs):
    '''
    
    what this is doing 
    input args
    output args or what it sets 
    example usage
    
    '''
    channel_id = engagement.get("customer_profile",{})
    channel_id = channel_id.get("source",None)

    split =  self.spliters(channel_id) if channel_id else self.spliters("test")

    response_string = kwargs.get("response_string")
    response_data = kwargs.get("sr_data",{})
    response_whiteboard = kwargs.get("response_whiteboard","")
    customer_state = kwargs.get("customer_state")
    if response_string:
        if "{split}" in response_string:
            response_string = response_string.replace("{split}",split)
    
    # formatting options for chatbot
    if not channel_id or channel_id in ["test","dave", "web","web-chatbot","facebook","whatsapp","nexa", "arena","kiosk","twitter","call"] :
        response_options = kwargs.get("response_options")
        if response_options and type(response_options) is list:
            response_data["response_type"] = "options"
            response_data["options"] = response_options
            
            if customer_state:
                response_data["options"] = [[customer_state,opt] for opt in response_options]
                response_data["customer_state"] = customer_state
            if "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}","")
            
            res_type = kwargs.get("response_type",None)
            res_data = kwargs.get("data", None)
            if res_type and res_data :
                if res_type == "thumbnails":
                    response_data = {}
                    response_data["response_type"] = "thumbnails" 
                    if res_data.get("thumbnails"):
                        response_data["thumbnails"] = res_data.get("thumbnails")
                    if res_data.get("slideshow"):
                        response_data["slideshow"] = res_data.get("slideshow")
            
            res_whiteboard = kwargs.get("whiteboard")
            if res_whiteboard:
                response_whiteboard = res_whiteboard
    
    # formatting options for social 
    elif channel_id in ["whatsapp", "instagram"]:
        response_options = kwargs.get("response_options")
        if response_options and type(response_options) is list:
            enumerated_options_list = enumerate(response_options)
            options_str_list = [u"{}. {}{}".format(k+1,v,split) for k,v in enumerated_options_list]
            options_str = "".join(options_str_list)
            if "{response_options}" in response_string:
                response_string = response_string.replace("{response_options}",options_str)
            else:
                response_string = u"{}{}{}".format(response_string, split, options_str)

    # formatting options for social

    elif channel_id in ["kiosk", "web3d","web-avatar"]:
        response_options = kwargs.get("response_options")
        if response_options and response_whiteboard in ["options","buttons"]:
            customer_state = kwargs.get("customer_state")
            if customer_state:
                buttons=[]
                for opt in response_options:
                    if type(opt)==list:
                        buttons.append({"customer_response" : opt[0],"customer_state" : customer_state, "title" : opt[1]})
                    else:
                        buttons.append({"customer_response" : opt,"customer_state" : customer_state, "title" : opt})
                response_data["buttons"] = buttons
        elif response_whiteboard == "popup_url":
            redirect_url = kwargs.get("redirect_url")
            hp.logger.info("redirect_url {}".format(redirect_url))
            if redirect_url:
                response_whiteboard = "popup_url"
                response_data["redirect"] = redirect_url
                hp.logger.info("response_whiteboard {}".format(response_whiteboard))
        elif response_whiteboard == "long_text":
            data=kwargs.get("long_text_data")
            if data:
                response_whiteboard ="long_text"
                response_data["title"]=data.get("title","")
                response_data["sub_title"]=data.get("sub_title","")
                response_data["description"]=data.get("description","")
                response_data["background"]=data.get("background","")
    engagement.set(response_string,"response")
    if response_whiteboard:
        engagement.set("{}.html".format(response_whiteboard),"reponse_whiteboard")
    elif engagement.get("default_whiteboard"):
        engagement.set("variant_info.html","reponse_whiteboard")
        engagement.set(engagement.get("default_whiteboard"),"variant_image")
    else :
        engagement.set("","reponse_whiteboard")
    engagement.set("static" if kwargs.get("custom_whiteboard") else "file" , "whiteboard_template")
    engagement.set(response_data if response_data else {},"response_data")

    return "sr_ask_options"
    # chatbot
    # response_type : "options"
    # "options": [
    #         <customer_response option>,
    #         <customer_response option>
    #      ],
    
    # kiosk
    # whiteboard = options.html
    # "whiteboard_template": "file"
    # "options": [
    #         <customer_response option>,
    #         <customer_response option>
    #      ],
    
def set_state_options(self, engagement, *args, **kwargs):
    state_options = kwargs.get("state_options")
    engagement.set( state_options, '_custom_state_options')
def get_option_list_from_filter_key_value(self,engagement, filter_list, name_dict):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_dict.keys()) is list:
        options_list = []
        for k,v in options:
            vals = ["{} : {}".format(valls,v.get(name_keyy)) for name_keyy,valls in name_dict.iteritems()]
            options_list.append(", ".join(vals))
        return options_list

def check_old_required(self, engagement, attr):
    return engagement.get("gl_old_{}".format(attr),None)

def reset_old_to_new_required(self, engagement, attr):
    self.add_variable_to_engagment(engagement,attr, engagement.get("gl_old_{}".format(attr)))
    engagement.save(None,"gl_old_{}".format(attr))

def get_option_list_from_filter_key_value(self,engagement, filter_list, name_dict):

    p,m,split = self.initial_attributes(engagement,required_models=[])
    options = enumerate(filter_list)
    if type(name_dict.keys()) is list:
        options_list = []
        for k,v in options:
            vals = ["{} : {}".format(valls,v.get(name_keyy)) for name_keyy,valls in name_dict.iteritems()]
            options_list.append(", ".join(vals))
        return options_list
        
    






  