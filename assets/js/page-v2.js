//Download Pdf
$(".viz-ic-download").on("click", function () {
  download_pdf();
});

jQuery.validator.addMethod(
  "validateEmailId",
  function (value, element) {
    return (
      this.optional(element) ||
      /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value)
    );
  },
  "Provide valid email id"
);

if (!localStorage.getItem("isFirstVisit")) {
  $("#orntModalHeader").html("Note");
  $("#orntModalbody").html(
    "The original product may vary from digital rendition"
  );
  $("#orientationModal").fadeIn();
  localStorage.setItem("isFirstVisit", true);
}

//Update password form validation
$(".update-password-form")
  .submit(function (e) {
    e.preventDefault();
  })
  .validate({
    rules: {
      up_otp: {
        required: true,
        minlength: 4,
        normalizer: function (value) {
          return $.trim(value);
        },
      },
      up_password: {
        required: true,
        minlength: 4,
        normalizer: function (value) {
          return $.trim(value);
        },
      },
      up_confirm_password: {
        required: true,
        minlength: 4,
        equalTo: "#up_password",
      },
    },
    messages: {
      up_otp: {
        required: "Please enter your otp",
        minlength: "Your otp must be at least 4 characters long",
      },
      up_password: {
        required: "Please enter your password",
        minlength: "Your password must be at least 4 characters long",
      },
      up_confirm_password: {
        required: "Please enter your password",
        minlength: "Your password must be at least 4 characters long",
        equalTo: "Confirm password must be same as password",
      },
    },

    submitHandler: function (form) {
      $("#upSubmitBtn").attr("disabled", true);
      $("#upSubmitBtn").text("Please wait...");
      let password = $("#up_password").val();
      let otp = $("#up_otp").val();
      let fp_email_id = $("#fp_email_id").val();

      /*let mobile_number = $("#fp_phone_number").val();
    let country_code = $("#fp_country_code").val();
    let phoneNumber = country_code+mobile_number;*/

      set_password(
        fp_email_id,
        otp,
        password,
        function (result) {
          console.log(result);
          if (result) {
            $("#auth-alert-result").html(
              '<div class="alert alert-success auth-alert">Your password has been updated successfully. Login to continue.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
            );
            $(".block-signin").show();
            $(
              ".block-signup, .block-forgot-password, .block-update-password"
            ).hide();
          }
          $("#upSubmitBtn").removeAttr("disabled");
          $("#upSubmitBtn").html("SUBMIT");
        },
        function (errorResult) {
          console.log(errorResult);
          $("#upSubmitBtn").removeAttr("disabled");
          $("#upSubmitBtn").html("SUBMIT");
          let displayError = "Invalid OTP.";
          $("#auth-alert-result").html(
            '<div class="alert alert-danger auth-alert">' +
              displayError +
              '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
          );
        }
      );
    },
  });

//forgot password form validation
$(".forgot-password-form").validate({
  rules: {
    /* fp_country_code: {
      required: true 
    },  
    fp_phone_number: {
      required: true,
      minlength: 10,
      maxlength: 10,
      number: true,
    },*/
    fp_email_id: {
      required: true,
      email: true,
      validateEmailId: true,
    },
  },
  messages: {
    fp_email_id: {
      required: "Please enter email id",
      email: "Please enter valid email id",
    },
    /*fp_country_code: {
      required: "Country is required",
       
    }, 
    fp_phone_number: {
      required: "Please enter a mobile number",
      minlength: "Mobile number must be at least 10 characters long",
      maxlength: "Mobile number must be less than 10 characters in length"
    }*/
  },
  submitHandler: function (form) {
    $("#fpSubmitBtn").attr("disabled", true);
    $("#fpSubmitBtn").text("Please wait...");
    let fp_email_id = $("#fp_email_id").val();

    /*let fp_phone_number = $("#fp_phone_number").val(); 
    let country_code = $("#fp_country_code").val(); 
    let phoneNumber = country_code+fp_phone_number; */

    forgot_password(
      fp_email_id,
      function (result) {
        if (result) {
          sendRealOtp("", fp_email_id);
          $("#auth-alert-result").html(
            '<div class="alert alert-success auth-alert">OTP has been sent. Update your password.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
          );
          $(".block-update-password").show();
          $(".block-signin, .block-signup, .block-forgot-password").hide();
        }
        $("#fpSubmitBtn").removeAttr("disabled");
        $("#fpSubmitBtn").html("SUBMIT");
      },
      function (errorResult) {
        $("#fpSubmitBtn").removeAttr("disabled");
        $("#fpSubmitBtn").html("SUBMIT");
        let displayError = "Invalid credentials.";
        if (errorResult.status == 404) {
          displayError = "Invalid email id";
        }
        $("#auth-alert-result").html(
          '<div class="alert alert-danger auth-alert">' +
            displayError +
            '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
      }
    );
  },
});

//Send forgot password OTP module
function sendFpOtpFunc() {
  /*let fp_phone_number = $("#fp_phone_number").val(); 
 let country_code = $("#fp_country_code").val(); 
 let phoneNumber = country_code+fp_phone_number; */

  let fp_email_id = $("#fp_email_id").val();

  forgot_password(
    fp_email_id,
    function (result) {
      if (result) {
        sendRealOtp("", fp_email_id);
        $("#auth-alert-result").html(
          '<div class="alert alert-success auth-alert">OTP has been sent.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
        $(".block-update-password").show();
        $(".block-signin, .block-signup, .block-forgot-password").hide();
      }
    },
    function (errorResult) {
      let displayError = "Invalid credentials.";
      $("#auth-alert-result").html(
        '<div class="alert alert-danger auth-alert">' +
          displayError +
          '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
      );
    }
  );
}

//Login form validation
$(".login-form").validate({
  rules: {
    /*login_phone_number: {
      required: true,
      minlength: 10,
      maxlength: 10,
      number: true,
    },*/
    login_email_id: {
      required: true,
      email: true,
      validateEmailId: true,
    },
    login_password: {
      required: true,
      minlength: 4,
      normalizer: function (value) {
        return $.trim(value);
      },
    },
  },
  messages: {
    login_email_id: {
      required: "Please enter email id",
      email: "Please enter valid email id",
    },
    login_password: {
      required: "Please enter your password",
      minlength: "Your password must be at least 4 characters long",
    },
  },
  submitHandler: function (form) {
    $("#loginSubmit").attr("disabled", true);
    $("#loginSubmit").text("Please wait...");

    let login_password = $("#login_password").val();
    let login_email_id = $("#login_email_id").val();
    /* let login_phone_number = $("#login_phone_number").val();
    let country_code = $("#login_country_code").val(); 
    let phoneNumber = country_code+login_phone_number; */

    login(
      login_email_id,
      login_password,
      function (result) {
        $("#auth-alert-result").html(
          '<div class="alert alert-success auth-alert">You are successfully logged in... <button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
        setTimeout(function () {
          window.location.reload();
        }, 2000);
      },
      function (errorResult) {
        $("#loginSubmit").removeAttr("disabled");
        $("#loginSubmit").html("LOGIN");
        let displayError = "Invalid credentials.";
        if (errorResult.status == 401) {
          displayError = "Invalid login credentials";
        }
        $("#auth-alert-result").html(
          '<div class="alert alert-danger auth-alert">' +
            displayError +
            '<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
      }
    );
  },
});

//get ountry codes
const getCountryCodes = async () => {
  try {
    const countryCodesApiResult = await fetch("assets/json/country_codes.json");
    codes = await countryCodesApiResult.json();
    let countryCode = "";
    codes.map((code) => {
      let countrySelected = code.name == "Canada" ? "selected" : "";
      countryCode += `<option ${countrySelected} value="${code.dial_code}">${code.name} (${code.dial_code})</option> `;
    });

    $(".countryCodes").html(countryCode);
  } catch (error) {
    console.log("Error : ", error);
  }
};
getCountryCodes();

//Validate user is logged in or not
if (logged_in) {
  $(".logoutBtn").show();
  $(".loginBtn").hide();
  //$(".bim-download").removeClass("loginBtn")
  $(".bim-download").attr(
    "href",
    "https://general-iamdave.s3.us-west-2.amazonaws.com/octopus_products/bim_file/3ds_max.zip"
  );
} else {
  $(".logoutBtn").hide();
  $(".loginBtn").show();
  //$(".bim-download").addClass("loginBtn")
  $(".bim-download").attr("href", "javascript:void(0)");
  $(".bim-download").click(() => {
    $("#orntModalHeader").html("Login is required");
    $("#orntModalbody").html("Please login to download.");
    $("#orientationModal").fadeIn();
  });
}

//logout use
$(".logoutBtn").on("click", function () {
  clearCookie("authentication");
  signup();
  logged_in = false;
  setCookie("logged_in", false);
  window.location.href = "index.html";
});

$(".btn-signup-send-otp").on("click", function () {
  sendOtpFunc();
  //$(".btn-signup-send-otp").hide();
  setTimeout(() => {
    $(".btn-signup-resend-otp").show();
  }, 5000);
});

$(".btn-signup-resend-otp").on("click", function () {
  sendOtpFunc();
});

$(".btn-up-resend-otp").on("click", function () {
  sendFpOtpFunc();
});

//Send OTP in signup, after completion of 10 digits
function sendOtpFunc() {
  $("#auth-alert-result").html("");
  //if($('#signup_phone_number').val().match('[0-9]{10}') && $('#signup_phone_number').val().length==10) {

  let emailId = $("#signup_email").val();
  let pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

  if ($("#signup_email").val().length && pattern.test(emailId)) {
    let mobileNumber =
      $("#signup_country_code").val() + $("#signup_phone_number").val();

    const signupEmailId = pattern.test(emailId) ? emailId : "";
    unique_customer(signupEmailId, function (result) {
      if (result) {
        sendRealOtp(mobileNumber, signupEmailId);
        $("#auth-alert-result").html(
          '<div class="alert alert-success auth-alert">OTP has been sent.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
      } else {
        $("#auth-alert-result").html(
          '<div class="alert alert-danger auth-alert">Email id is already exists.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
      }
    });
  } else {
    $("#auth-alert-result").html(
      '<div class="alert alert-danger auth-alert">Provide valid email id.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
    );
  }
}

//Send OTP API
function sendRealOtp(mobileNumber, toEmailId) {
  send_otp(mobileNumber, toEmailId);
}

//Signup form validation

$(".signup-form").validate({
  rules: {
    signup_name: {
      required: true,
      minlength: 2,
      maxlength: 40,
      normalizer: function (value) {
        return $.trim(value);
      },
    },
    signup_email: {
      required: true,
      email: true,
      validateEmailId: true,
    },
    signup_country_code: {
      normalizer: function (value) {
        return $.trim(value);
      },
    },
    signup_phone_number: {
      minlength: 10,
      maxlength: 10,
      number: true,
    },
    signup_password: {
      required: true,
      minlength: 4,
      normalizer: function (value) {
        return $.trim(value);
      },
    },
    signup_confirm_password: {
      required: true,
      minlength: 4,
      equalTo: "#signup_password",
    },
    signup_otp: {
      required: true,
      minlength: 4,
      normalizer: function (value) {
        return $.trim(value);
      },
    },
  },
  messages: {
    signup_name: {
      required: "Please enter a name",
      minlength: "Name must consist of at least 2 characters",
    },
    signup_email: {
      required: "Please enter email id",
      email: "Provide valid email id",
    },
    signup_country_code: {
      required: "Please enter country",
    },
    signup_phone_number: {
      required: "Please enter a mobile number",
      minlength: "Mobile number must be at least 10 characters long",
      maxlength: "Mobile number must be less than 10 characters in length",
    },
    signup_password: {
      required: "Please enter your password",
      minlength: "Your password must be at least 4 characters long",
    },
    signup_confirm_password: {
      required: "Please enter your password",
      minlength: "Your password must be at least 4 characters long",
      equalTo: "Confirm password must be same as password",
    },
    signup_otp: {
      required: "Please enter your otp",
      minlength: "Your otp must be at least 4 characters long",
    },
  },
  submitHandler: function (form) {
    $("#signUpSubmitBtn").attr("disabled", true);
    $("#signUpSubmitBtn").text("Please wait...");
    let name = $("#signup_name").val();
    let email = $("#signup_email").val();
    let mobile_number = $("#signup_phone_number").val();
    let password = $("#signup_password").val();
    let country_code = $("#signup_country_code").val();
    let signup_confirm_password = $("#signup_confirm_password").val();
    let otp = $("#signup_otp").val();
    let phoneNumber = country_code + mobile_number;

    unique_customer(email, function (result) {
      if (result) {
        update_customer(
          {
            name: name,
            email: email,
            phone_number: phoneNumber,
            password: password,
            otp: otp,
            made_enquiry: true,
          },
          function (result) {
            setCookie("logged_in", true);
            logged_in = true;
            $("#auth-alert-result").html(
              '<div class="alert alert-success auth-alert">Registration has been completed successfully.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
            );
            setTimeout(function () {
              window.location.reload();
            }, 2000);
          }
        );
      } else {
        $("#auth-alert-result").html(
          '<div class="alert alert-danger auth-alert">Mobile number is already exists.<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
        );
      }
      $("#signUpSubmitBtn").removeAttr("disabled");
      $("#signUpSubmitBtn").html("SIGN UP");
    });
  },
});

//Share login form validation
$(".share-login-form").validate({
  rules: {
    share_name: {
      required: true,
      minlength: 2,
      normalizer: function (value) {
        return $.trim(value);
      },
    },
    share_mobile_number: {
      required: true,
      minlength: 10,
      maxlength: 10,
      number: true,
    },
    share_email_id: {
      required: true,
      email: true,
    },
  },
  beforeSubmit: function (arr, $form, options) {
    $(".loader_submit").show();
    return true;
  },
  messages: {
    share_mobile_number: {
      required: "Please enter a mobile number",
      minlength: "Mobile number must be at least 10 characters long",
      maxlength: "Mobile number must be less than 10 characters in length",
    },
    share_name: {
      required: "Please enter your name",
      minlength: "Name must be at least 2 characters long",
    },
    share_email_id: {
      required: "Please enter your email",
      email: "provide valid email id",
    },
  },
  submitHandler: function (form) {
    $("#submitShareLogin").attr("disabled", true);
    $("#submitShareLogin").text("Please wait...");
    let mobile_number = $("#share_mobile_number").val();
    let name = $("#share_name").val();
    let email = $("#share_email_id").val();
    update_customer(
      {
        name: name,
        email: email,
        phone_number: mobile_number,
        made_enquiry: true,
      },
      function (result) {
        showShareBlock();
        $("#loginShareModalPopup").fadeOut();
        $("#submitShareLogin").removeAttr("disabled");
        $("#submitShareLogin").html("SUBMIT");
      }
    );
  },
});

//Social Share
$(".social-share").on("click", function () {
  const link = $(this).data("social_share");
  if (link == "facebook") {
    window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(
        window.location.href
      )}&quote=Please click this link to have a look at the designs I have selected`
    );
  } else if (link == "twitter") {
    window.open(
      `https://twitter.com/intent/tweet?via=${encodeURIComponent(
        window.location.href
      )}&text=Please click this link to have a look at the designs I have selected`
    );
  } else if (link == "linkedin") {
    window.open(
      `https://www.linkedin.com/shareArticle?url=${encodeURIComponent(
        window.location.href
      )}&title=Please click this link to have a look at the designs I have selected&summary=Please click this link to have a look at the designs I have selected`
    );
  } else if (link == "pinterest") {
    window.open(
      `http://pinterest.com/pin/create/button/?url=${encodeURIComponent(
        window.location.href
      )}&media=https://octopus-products.iamdave.ai/assets/images/logo.PNG&description=Please click this link to have a look at the designs I have selected`
    );
  }
});

$(".asbGoBack a").on("click", function () {
  $(".asb_categories").prop("checked", false);
  $(".advanceSearchResult, .asbSubCategory").html("");
  $("#loadMoreAdvanceSearch , .asbGoBack , .asbSubCategoryTitle").hide();
  $(".advance-search-category").show();
});

//Advance filter
function filterCategory() {
  $(".asbSubCategory").html("");
  $(".asbSubCategoryTitle , .asbSubCategory").hide();

  /*$(this).prop("checked", true);
 getAdvanceSearchSubCategories( $(this).val() ); 
 $(".asbSubCategoryTitle , .asbSubCategory").show(); */
  var categories = [];
  $.each($("input[name='asb_categories']:checked"), function () {
    let selectedCategory = $(this).val();
    localStorage.setItem("selectedDoorCategory", selectedCategory);
    //if(!validateIsDoor(selectedCategory)){ return false; }
    getAdvanceSearchSubCategories($(this).val());
    categories.push($(this).val());
  });
  if (categories.join(",")) {
    $(".asbSubCategoryTitle , .asbSubCategory").show();
  }
}
  
//Advance sub category filter
const getAdvanceSearchSubCategories = async (selectedCategory) => {
  try {
    const ApiRes = await fetch("assets/json/visualizer_spreadsheet.json");
    categories = await ApiRes.json();
    let filteredCategory = categories.find((category) => {
      if (selectedCategory == category.category_name) {
        return JSON.stringify(category);
      }
    });
    let subCategories = filteredCategory.sub_category;
    subCategories.map((subCategory) => {
      let asbSubCategories = `<div class="checkbox">
          <label>${subCategory.sub_category_name}<input type="checkbox" value="${subCategory.sub_category_name}" onclick="getAdvanceSearchDesign('${selectedCategory}', '${subCategory.sub_category_name}', 1)"></label>
       </div>
       `;
      $(".asbSubCategory").append(asbSubCategories);
    });
  } catch (error) {
    console.log("Error : ", error);
  }
};

//Get advance filter design
function getAdvanceSearchDesign(
  selectedCategory,
  selectedSubCategory,
  page_number = 1
) {
  /*$(".advance-search-section,.remove-all-section, .btn-close").hide(); 
 $(".main-categories-section").show();
 $(".section-common, .btn-close").hide();   
 $(".btn-advance-search").hide();
 $(".categories-section, .remove-all-section, .btn-category-close").show();*/
  $(".advance-search-category").hide();
  $(".asbGoBack").show();
  $(".asbGoBack a").html(
    `<span class="glyphicon glyphicon-circle-arrow-left"></span> ${selectedSubCategory}`
  );
  $("#loadMoreAdvanceSearch")
    .show()
    .html(`<img src="assets/images/loader.gif" style="width: 40px;">`);
  get_designs(
    {
      category_name: selectedCategory,
      sub_category_name: selectedSubCategory,
      _page_size: 20,
      _page_number: page_number,
    },
    function (result) {
      let {
        is_first,
        total_number,
        page_size,
        page_number,
        is_last,
        data: designs,
      } = result;
      if (is_last) {
        $("#loadMoreAdvanceSearch").html("");
      }
      if (page_number == 1) {
        $(".advanceSearchResult").html("");
      }
      designs.map((design) => {
        let productKey = design.product_id;
        productKey = productKey.replace(/ /g, "_");
        let singleDesign = `<div class="col-md-6 col-sm-6 col-xs-6">
            <a href="javascript:void(0)" onclick="callChange('${productKey}', '${design.finish}', this)">
                <div class="mds_viewed_image" data-product_id = '${productKey}' data-finish = '${design.finish}'>
                    <img class="img-responsive size-140x140" src="${design.thumbnail}">
                    <p>${design.product_id} ${design.product_name}</p>
                </div>
            </a>
            <a class="downloadbimonactive" id="bimactive_${productKey}" download="${design.product_id} ${design.product_name}" href="https://general-iamdave.s3.us-west-2.amazonaws.com/octopus_products/bim_file/sketchup/${design.product_id}.skm"><img style="width: 50px;" src="assets/images/Icons/activefile.png"></a>
          </div>`;
        $(".advanceSearchResult").append(singleDesign);
      });
      if (!is_last) {
        let loadMoreBtn = `<div class="col-md-12 text-center"><a href="javascript:void(0)" onclick="getAdvanceSearchDesign('${selectedCategory}', '${selectedSubCategory}', ${++page_number})" class="btn btn-load-more">Load More</a></div>`;
        $("#loadMoreAdvanceSearch").html(loadMoreBtn);
      }
    }
  );
}

const parameters = get_url_params();
const roomId = parameters["page_id"];

$(window).on("load", function () {
  displayWishlist();
  $("#favorite-list").html(
    `<p class='text-center'><img src="assets/images/loader.gif" style="width:25px" alt="Please wait.."/></p>`
  );
});

//Display wishlist designs
function do_add_view(wishlistResult, elem) {
  $(elem).html("");

  $(".wishlistBefore").hide();
  $(".wishlistAfter").show();

  if (!wishlistResult.total_number) {
    $(elem).html(
      "<p class='text-center'>Please add some designs to your wishlist first</p>"
    );
    $(".wishlistBefore").show();
    $(".wishlistAfter").hide();
  }

  for (let d of wishlistResult.data) {
    //Convert selection parameter to url
    var wishlistUrl = [];
    let urlData = d.selection;
    for (var key in urlData) {
      wishlistUrl.push(key + "=" + urlData[key]);
    }
    wishlistUrl = wishlistUrl.join("&");

    let di = $(`
                <div class="fav-b interaction wishlist-item"> 
                   <div class="row">
                      <div class="col-md-6">
                         <div class="img-thumb"></div> 
                      </div> 
                      <div class="col-md-6"> 
                         <p class="cross-fv"><a href="javascript:void(0)" class="delete-wishlist" data-wishlist_id="${d.wishlist_id}">x</a></p>
                         <div class="fv-content"> 
                            <br/> 
                            <span>${d.view_name}</span>
                            <p ><a class="btn btn-info fv-shr" href="page.html?${wishlistUrl}"> <span class="glyphicon glyphicon-eye-open"></span> View</a></p>
                         </div>
                      </div> 
                   </div>  
                </div>  
        `);
    set_image(
      init_image_list(d.room_id, d.view_id, d.selection),
      di.find(".img-thumb")
    );
    $(elem).append(di);
    /*$( di ).find(".ic-delete-section").data("selection", d.selection)*/
  }
  $(".delete-wishlist")
    .off()
    .on("click", function () {
      $(".loader").show();
      let that = this;
      delete_wishlist($(this).data("wishlist_id"), function () {
        displayWishlist();
        $(that).closest(".wishlist-item").remove();
        $(".loader").hide();
        /*let parent_elem = $( '#wishlistCount' ).data('current_tab');
            $( parent_elem ).data('number', $( parent_elem ).data('number') - 1);
            $( '#wishlistCount' ).text($( parent_elem ).data('number'))*/
      });
    });

  /*$( ".ic-delete-section" ).off().on("click", function() {
        download_pdf($( this ).data('room_id'), $( this ).data('view_id'), $( this ).data('selection'), $( this ).parent().parent() );
    });*/
}

function displayWishlist() {
  get_space_wishlist(
    { _page_number: 1, _page_size: 20 },
    function (wishlistResult) {
      let elem = $("#favorite-list");
      do_add_view(wishlistResult, elem);
    }
  );
}

function update_total_shortlist(count) {
  //displayWishlist();
  /*if ( $( "#wishlistCount" ).is(":visible") ) {
       count = parseInt($( "#wishlistCount" ).text()) + count;
  }
  if ( count <= 0 ) {
      $( "#wishlistCount" ).hide();
  } else {
      $( "#wishlistCount" ).show();
      $( "#wishlistCount" ).text(count);
  }*/
}

$(document).ready(function () {
  var mainSearchText = document.getElementById("main-search-text");
  mainSearchText.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      doProductSearch();
    }
  });
});

$(document).ready(function () {
  is_view_in_wishlist(
    makeSingle(get_url_params()["page_id"]),
    makeSingle(get_url_params()["view_id"]),
    function (interaction_id) {
      $(".w-added").first().data("interaction_id", interaction_id);
    }
  );
  $(".wishlist").on("click", function () {
    $(".loader").show();
    let that = $(this);
    if ($(".w-wish").is(":visible")) {
      add_view_to_wishlist(function (data) {
        $(that).find(".w-wish").hide();
        $(that).find(".w-added").show();
        if (
          data.wishlist_id !=
          $(that).find(".w-added").first().data("interaction_id")
        ) {
          update_total_shortlist(1);
        }
        $(that)
          .find(".w-added")
          .first()
          .data("interaction_id", data.wishlist_id);
        $(".loader").hide();
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }, $(that).find(".w-added").first().data("interaction_id"));

      //displayWishlist();
    } else {
      delete_wishlist(
        $(that).find(".w-added").first().data("interaction_id"),
        function () {
          $(that).find(".w-wish").show();
          $(that).find(".w-added").hide();
          update_total_shortlist(-1);
          $(".loader").hide();
          setTimeout(function () {
            window.location.reload();
          }, 1);
        }
      );
    }
  });

  /*get_total_shortlist(update_total_shortlist);
  $( ".viz-ic-download" ).on("click", function() {download_pdf()})*/
});

//Get rooms for dropdown menu
const getRooms = async () => {
  try {
    const roomApiResult = await fetch("assets/json/rooms.json");
    rooms = await roomApiResult.json();
    let slNumber = 1;
    let singleRoom = "";
    const totalRooms = rooms.length;
    rooms.map((room) => {
      let startDiv = (endDiv = midDiv = "");
      if (slNumber == 1) {
        startDiv = `<div class="mMsR">`;
      }
      if (slNumber % 3 == 0 && totalRooms != slNumber) {
        midDiv = `</div><div class="mMsR">`;
      }
      if (totalRooms == slNumber) {
        endDiv = `</div>`;
      }
      singleRoom += `${startDiv} 
          <li class="col-sm-4 col-xs-4">
              <ul>
                 <li>
                    <a href="page.html?page_id=${room.id}">
                      <img class="img-responsive ${
                        room.id == roomId ? "active" : ""
                      }" src="${room.thumbnail}"/>
                      <p>${room.title}</p>
                    </a>
                  </li>
              </ul>
          </li>${midDiv} ${endDiv}`;
      slNumber++;
    });
    $(".menu-office-data").append(singleRoom);
  } catch (error) {
    console.log("Error : ", error);
  }
};
getRooms();

//Get room view for dropdown menu
const getViews = async (roomId) => {
  try {
    const roomApiResult = await fetch("assets/json/rooms.json");
    rooms = await roomApiResult.json();

    const selectedRoom = rooms.find((room) => {
      if (roomId == room.id) return room;
    });
    const roomViews = selectedRoom.views;

    $(".btn-office-name").html(selectedRoom.title);

    let slNumber = 1;
    let singleView = "";
    const totalRooms = roomViews.length;
    roomViews.map((view) => {
      let startDiv = (endDiv = midDiv = "");
      if (slNumber == 1) {
        startDiv = `<div class="mMsR">`;
      }
      if (slNumber % 3 == 0 && totalRooms != slNumber) {
        midDiv = `</div><div class="mMsR">`;
      }
      if (totalRooms == slNumber) {
        endDiv = `</div>`;
      }
      singleView += `${startDiv} 
             <li class="col-sm-4 col-xs-4">
                 <ul>
                    <li>
                      <a href="page.html?page_id=${roomId}&view_id=${view.id}">
                         <img class="img-responsive ${
                           view.id == view_id ? "active" : ""
                         }" src="${view.thumbnail}"/>
                         <p>${view.title}</p>
                       </a>
                     </li>
                 </ul>
             </li>${midDiv} ${endDiv}`;
      slNumber++;
    });
    if (!singleView) {
      $(".menu-full-view-data").html(
        "<p class='text-center'>No view found!.</p>"
      );
    } else {
      $(".menu-full-view-data").append(singleView);
    }
  } catch (error) {
    console.log("Error : ", error);
  }
};
getViews(roomId);

//Get designs
function callGetDesign(selectedCategory, selectedSubCategory, page_number = 1) {
  // For horizontal and vertical buttons
  if (selectedCategory == "Octolam" && selectedSubCategory == "Woodgrain") {
    $(".hvBtns").show();
  } else {
    $(".hvBtns").hide();
  }

  $("#loadMoreSection").html(
    `<img src="assets/images/loader.gif" style="width: 40px;">`
  );
  get_designs(
    {
      category_name: selectedCategory,
      sub_category_name: selectedSubCategory,
      _page_size: 20,
      _page_number: page_number,
    },
    function (result) {
      let {
        is_first,
        total_number,
        page_size,
        page_number,
        is_last,
        data: designs,
      } = result;
      if (is_last) {
        $("#loadMoreSection").html("");
      }
      if (page_number == 1) {
        $("#main-categories-data").html("");
      }
      $(".btn-categories, .category-icon").hide();
      $(".categories-pagination").html(` 
           <a href="javascript:void(0)" class="sC" onclick='getSubCategories("${selectedCategory}")'><span class="glyphicon glyphicon-circle-arrow-left"></span> ${selectedSubCategory}
           </a> 
        `); 
      designs.map((design) => {
        let productKey = design.product_id;
        productKey = productKey.replace(/ /g, "_");
        let singleDesign = `<div class="col-md-6 col-sm-6 col-xs-6">
                <a href="javascript:void(0)" onclick="callChange('${productKey}', '${design.finish}', this)">
                    <div class="mds_viewed_image" data-product_id = '${productKey}' data-finish = '${design.finish}'>
                        <img class="img-responsive size-140x140" src="${design.thumbnail}">
                        <p>${design.product_id} ${design.product_name}</p>
                    </div>
                </a>
                <a class="downloadbimonactive" id="bimactive_${productKey}" download="${productKey} ${design.product_name}" href="https://general-iamdave.s3.us-west-2.amazonaws.com/octopus_products/bim_file/sketchup/${productKey}.skm"><img style="width: 50px;" src="assets/images/Icons/activefile.png"></a>
              </div>`;
        $("#main-categories-data").append(singleDesign);
      });
      if (!is_last) {
        let loadMoreBtn = `<div class="col-md-12 text-center"><a href="javascript:void(0)" onclick="callGetDesign('${selectedCategory}', '${selectedSubCategory}', ${++page_number})" class="btn btn-load-more">Load More</a></div>`;
        $("#loadMoreSection").html(loadMoreBtn);
      }
    }
  );
}

//Get categories for sidebar
const getMainCategories = async () => {
  try {
    $(".btn-categories, .category-icon").show();
    $(
      "#main-categories-data , .categories-pagination, #loadMoreSection, .asbCategory"
    ).html("");
    const ApiRes = await fetch("assets/json/visualizer_spreadsheet.json");
    categories = await ApiRes.json();
    categories.map((category) => {
      let singleCategory = `<div class="col-md-6 col-sm-6 col-xs-6">
              <a href="javascript:void(0)" onclick=getSubCategories("${category.category_name}")>
                <img class="img-responsive size-140x140" src="${category.image}">
                <p>${category.category_name}</p>
              </a>
          </div>`;
      $("#main-categories-data").append(singleCategory);

      let searchCategory = ` 
          <div class="checkbox">
             <label>${category.category_name} <input type="radio" value="${category.category_name}" onchange="filterCategory()" name="asb_categories" class="asb_categories"></label>
          </div>
          `;
      $(".asbCategory").append(searchCategory);
    });
  } catch (error) {
    console.log("Error : ", error);
  }
};
getMainCategories();

//Get sub categories for sidebar
const getSubCategories = async (selectedCategory) => {
  try {
    $(".hvBtns").hide();
    localStorage.setItem("selectedDoorCategory", selectedCategory);
    //if(!validateIsDoor(selectedCategory)){ return false; }
    const ApiRes = await fetch("assets/json/visualizer_spreadsheet.json");
    categories = await ApiRes.json();
    let filteredCategory = categories.find((category) => {
      if (selectedCategory == category.category_name) {
        return JSON.stringify(category);
      }
    });
    let subCategories = filteredCategory.sub_category;
    $("#main-categories-data, #loadMoreSection").html("");
    $(".btn-categories, .category-icon").hide();
    $(".categories-pagination").html(` 
           <a href="javascript:void(0)" class="sC" onclick='getMainCategories()'><span class="glyphicon glyphicon-circle-arrow-left"></span> ${selectedCategory}
           </a>
        `);
    subCategories.map((subCategory) => {
      let singleSubCategory = `<div class="col-md-6 col-sm-6 col-xs-6">
                  <a href="javascript:void(0)" onclick="callGetDesign('${selectedCategory}', '${subCategory.sub_category_name}', 1)">
                    <img class="img-responsive size-140x140" src="${subCategory.image}">
                    <p>${subCategory.sub_category_name}</p>
                  </a>
              </div>`;
      $("#main-categories-data").append(singleSubCategory);
    });
  } catch (error) {
    console.log("Error : ", error);
  }
};
