let options = rooms: [
    {
      "id": "elevator_lobby",
      "title": "Elevator Lobby",
      "thumbnail": "/assets/images/2dviz/rooms/elevator_lobby.jpg",
      "views": [
          {
              "id": "master_view",
              "title": "Master View",
              "thumbnail": "/assets/images/2dviz/rooms/elevator_lobby.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/rooms/holed_out.png",
                  },
                  {
                    "id": "lift_top_wall",
                    "title": "Lift Top Wall", 
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/rooms/lift_top_wall.jpg",
                    "options": [],
                  },
                  {
                    "id": "lift_center_wall",
                    "title": "Lift Center Wall", 
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/rooms/elevator_lobby_2.jpg",
                    "options": [],
                  },
                  {
                    "id": "side_walls",
                    "title": "Side Walls", 
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/rooms/elevator_lobby_3.jpg",
                    "options": [],
                  }
              ]
          }
      ]
    }, 
    {
        "id": "spa",
        "title": "Spa",
        "thumbnail": "/assets/images/2dviz/rooms/spa.png",
        "views": [
            {
                "id": "master_view",
                "title": "Master View",
                "thumbnail": "/assets/images/2dviz/rooms/spa.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    }, 
                    {
                      "id": "wall_cladding",
                      "title": "Wall Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wall_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "lower_wall_cladding",
                      "title": "Lower Wall Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/spa_2.png",
                      "options": [],
                    } 
                ]
            }
        ]
    },
    {
        "id": "restaurant",
        "title": "Restaurant",
        "thumbnail": "/assets/images/2dviz/rooms/restaurant.png",
        "views": [
            { 
                "id": "master_view",
                "title": "Master View",
                "thumbnail": "/assets/images/2dviz/rooms/restaurant.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    }, 
                    {
                      "id": "counter_cladding",
                      "title": "Counter Cladding",   
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/restaurant_1.png",
                      "options": [],
                    },
                    {
                      "id": "table",
                      "title": "Table", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/restaurant_2.png",
                      "options": [],
                    }, 
                    {
                      "id": "wall_cladding",
                      "title": "Wall Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/restaurant_3.png",
                      "options": [],
                    },
                    {
                      "id": "shelf_cladding",
                      "title": "Shelf Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/restaurant_4.png",
                      "options": [],
                    }, 
                    {
                      "id": "counter_top",
                      "title": "Counter Top", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/restaurant_5.png",
                      "options": [],
                    }  
                ]
            },
            { 
                "id": "counter_top",
                "title": "Counter Top",
                "thumbnail": "/assets/images/2dviz/rooms/counter_top.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    }, 
                    {
                      "id": "counter_top",
                      "title": "Counter Top", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/counter_top_1.png",
                      "options": [],
                    }   
                ]
            },
            { 
                "id": "table",
                "title": "Table",
                "thumbnail": "/assets/images/2dviz/rooms/table.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    }, 
                    {
                      "id": "table",
                      "title": "Table", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/table_1.png",
                      "options": [],
                    }   
                ]
            }
        ]
    },
    {
        "id": "hotel_room",
        "title": "Hotel Room",
        "thumbnail": "/assets/images/2dviz/rooms/hotel_room.jpeg",
        "views": [
            {
                "id": "master_view",
                "title": "Master View",
                "thumbnail": "/assets/images/2dviz/rooms/hotel_room.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "tv_cladding",
                      "title": "TV Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/tv_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "wardrobe",
                      "title": "Wardrobe", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wardrobe.png",
                      "options": [],
                    },
                    {
                      "title": "Wall Cladding",
                      "id": "wall_cladding",
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wall_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "cabinets",
                      "title": "Cabinets", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/cabinets.png",
                      "options": [],
                    }, 
                ]
            }, 
            {
                "id": "bed_wall_cladding_cabinets",
                "title": "Bed/Wall Cladding/ Cabinets",
                "thumbnail": "/assets/images/2dviz/rooms/bed_wall_cladding_cabinets.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "wall_cladding",
                      "title": "Wall Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wall_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "wall_cladding_2",
                      "title": "Wall Cladding 2", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wall_cladding_2.png",
                      "options": [],
                    },
                    {
                      "id": "cabinets",
                      "title": "Cabinets", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/cabinets.png",
                      "options": [],
                    }, 
                ]
            }, 
            {
                "id": "wardrobe",
                "title": "Wardrobe",
                "thumbnail": "/assets/images/2dviz/rooms/wardrobe.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "wardrobe",
                      "title": "Wardrobe", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/wardrobe.png",
                      "options": [],
                    } 
                ]
            },
            {
                "id": "tv_unit",
                "title": "TV unit",
                "thumbnail": "/assets/images/2dviz/rooms/tv_unit.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "tv_cladding",
                      "title": "TV Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/tv_cladding.png",
                      "options": [],
                    } 
                ]
            } 
        ] 
    },


    {
        "id": "residential_cabinet",
        "title": "Residential Cabinet",
        "thumbnail": "/assets/images/2dviz/rooms/residential_cabinet.png",
        "views": [
            {
                "id": "master_view",
                "title": "Master View",
                "thumbnail": "/assets/images/2dviz/rooms/residential_cabinet.png",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    }, 
                    {
                      "id": "cabinet",
                      "title": "Cabinet", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/cabinet.png",
                      "options": [],
                    },
                    {
                      "id": "metal_trims",
                      "title": "Metal Trims", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/metal_trims.png",
                      "options": [],
                    } 
                ]
            }
        ]
    }, 
    {
        "id": "corridor",
        "title": "Corridor",
        "thumbnail": "/assets/images/2dviz/rooms/corridor.jpeg",
        "views": [
            {
                "id": "master_view",
                "title": "Master View",
                "thumbnail": "/assets/images/2dviz/rooms/corridor.jpeg",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "door_cladding",
                      "title": "Door Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/door_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "door",
                      "title": "Door", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/door.png",
                      "options": [],
                    } 
                ]
            },
            {
                "id": "door_view",
                "title": "Door View",
                "thumbnail": "/assets/images/2dviz/rooms/door_view.jpeg",
                "surfaces": [
                    {
                      "name": "holed_out",
                      "variable": false,
                      "init": "/assets/images/2dviz/rooms/holed_out.png",
                    },  
                    {
                      "id": "door_cladding",
                      "title": "Door Cladding", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/door_cladding.png",
                      "options": [],
                    },
                    {
                      "id": "door",
                      "title": "Door", 
                      "variable": true,
                      "left": "calc( 0.58*100vw )",
                      "top": "calc( 0.26*0.67*100vw )",
                      "init": "/assets/images/2dviz/rooms/door.png",
                      "options": [],
                    }
                ]
            }
        ]
    }, 
    {
        "id": "kitchen",
        "title": "Kitchen",
        "thumbnail": "/assets/images/2dviz/rooms/kitchen.jpeg",
        "views": []
    },

    {
        "id": "hotel_reception",
        "title": "Hotel Reception",
        "thumbnail": "/assets/images/2dviz/rooms/hotel_reception.jpeg",
        "views": []
    },
    {
        "id": "retail_store",
        "title": "Retail Store",
        "thumbnail": "/assets/images/2dviz/rooms/retail_store.jpeg",
        "views": []
    },
    {
        "id": "hospital_reception",
        "title": "Hospital Reception",
        "thumbnail": "/assets/images/2dviz/rooms/hospital_reception.jpeg",
        "views": []
    }
]