function detectMob() {
  let orntModalHeader = `Use Landscape View`;
  let orntModalbody = `<img src="assets/images/screen_rotation.png" class="mb-25">
               <p>Please rotate your device to get better view of the visualizer.</p>`;
  $("#orntModalHeader").html(orntModalHeader);
  $("#orntModalbody").html(orntModalbody);
  if ($(window).width() < $(window).height()) {
    $("#orientationModal").fadeIn(100);
  } else {
    $("#orientationModal").fadeOut(100);
    $(".modal-backdrop").hide();
  }
} 
$(".close_c_modal").on("click", function () {
  $(".modal").fadeOut();
  $(".modal-backdrop").hide();
  $(".loader").hide();
});
$(window).resize(detectMob);
detectMob();
$(".btn-close-auth").on("click", function () {
  $(".auth-dropdown").removeClass("show-auth-dropdown-menu");
});
$(".btn-forgot-password, .btn-change-number").on("click", function () {
  $(".block-forgot-password").show();
  $(".block-signup, .block-signin, .block-update-password").hide();
});
$(".btn-sign-in").on("click", function () {
  $(".block-signin").show();
  $(".block-signup, .block-forgot-password, .block-update-password").hide();
});
$(".btn-sign-up").on("click", function () {
  $(".block-signup").show();
  $(".block-forgot-password, .block-signin, .block-update-password").hide();
});
$(".btn-nav-auth").on("click", function () {
  $(".full-view-dropdown").removeClass("show-dropdown-menu-2");
  $(".share-dropdown").removeClass("show-share-dropdown-menu");
  $(".office-dropdown").removeClass("show-dropdown-menu-1");
  if ($(".auth-dropdown").hasClass("show-auth-dropdown-menu")) {
    $(".auth-dropdown").removeClass("show-auth-dropdown-menu");
  } else {
    $(".auth-dropdown").addClass("show-auth-dropdown-menu");
  }
});

$(".navbar-toggle").on("click", function () {
  if (!$(".navbar-collapse").hasClass("in")) {
    $(".left-sidebar").addClass("top-margin");
  } else {
    $(".left-sidebar").removeClass("top-margin");
  }
});
$(".btn-main").on("click", function () {
  if (!$(".navbar-collapse").hasClass("in")) {
    $(".navbar-collapse").addClass("in");
    $(".left-sidebar, .toggleSidebar").addClass("top-margin");
  } else {
    $(".navbar-collapse").removeClass("in");
    $(".left-sidebar, .toggleSidebar").removeClass("top-margin");
  }
  $(".btn-main").hide();
  $(".left-sidebar, .navbar-default, .toggleSidebar").show();
});

$(".btn-close-all-menu").on("click", function () {
  $(".btn-main").show();
  $(".left-sidebar, .navbar-default, .toggleSidebar").hide();
});
$(".btn-office").on("click", function () {
  $(".full-view-dropdown").removeClass("show-dropdown-menu-2");
  $(".share-dropdown").removeClass("show-share-dropdown-menu");
  if ($(".office-dropdown").hasClass("show-dropdown-menu-1")) {
    $(".office-dropdown").removeClass("show-dropdown-menu-1");
  } else {
    $(".office-dropdown").addClass("show-dropdown-menu-1");
  }
});
$(".btn-full-view").on("click", function () {
  $(".office-dropdown").removeClass("show-dropdown-menu-1");
  $(".share-dropdown").removeClass("show-share-dropdown-menu");
  if ($(".full-view-dropdown").hasClass("show-dropdown-menu-2")) {
    $(".full-view-dropdown").removeClass("show-dropdown-menu-2");
  } else {
    $(".full-view-dropdown").addClass("show-dropdown-menu-2");
  }
});
$(".close-office-dropdown").on("click", function () {
  $(".office-dropdown").removeClass("show-dropdown-menu-1");
  $(".full-view-dropdown").removeClass("show-dropdown-menu-2");
});
$(".btn-resize-full").on("click", function () {
  $(".left-sidebar, .navbar, #rooms, .toggleSidebar").hide();
  $(".btn-resize-small").show();
});
$(".btn-resize-small").on("click", function () {
  $(".left-sidebar, .navbar, #rooms, .toggleSidebar").show();
  $(".btn-resize-small").hide();

  $(".glyphiconCollapseLS").removeClass(
    "glyphicon-chevron-left glyphicon-chevron-right"
  );
  $(".glyphiconCollapseLS").addClass("glyphicon-chevron-left");
  $(".toggleSidebar").removeClass("tSToLeft");
});
$(document).ready(function () {
  $(".a-btn").on("click", function () {
    if ($(this).find(".fa").hasClass("fa-caret-down")) {
      $(this).find(".fa").removeClass("fa-caret-down").addClass("fa-caret-up");
    } else {
      $(this).find(".fa").removeClass("fa-caret-up").addClass("fa-caret-down");
    }
  });
});
$(".btn-close-share").on("click", function () {
  $(".share-dropdown").removeClass("show-share-dropdown-menu");
});

$(".btn-nav-share").on("click", function () {
  if (!logged_in) {
    $("#loginShareModalPopup").fadeIn();
    return false;
  }
  showShareBlock();
});

function showShareBlock() {
  $(".full-view-dropdown").removeClass("show-dropdown-menu-2");
  $(".office-dropdown").removeClass("show-dropdown-menu-1");
  $(".auth-dropdown").removeClass("show-auth-dropdown-menu");
  if ($(".share-dropdown").hasClass("show-share-dropdown-menu")) {
    $(".share-dropdown").removeClass("show-share-dropdown-menu");
  } else {
    $(".share-dropdown").addClass("show-share-dropdown-menu");
  }
}

/*$('.btn-share').click(function(){
  $(".section-common, .main-categories-section").hide();
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    $(".btn-favorite, .btn-share, .btn-categories").removeClass('active');
    $(".share-section,.remove-all-section").hide();
    $(".main-categories-section").show();
  } else {
    $('.btn-share').removeClass('active');
    $(this).addClass('active');
    $(".btn-favorite, .btn-categories").removeClass('active');
    $(".share-section,.remove-all-section").show();
  }
});*/

$(".btn-search-close").click(function () {
  $(".btn-advance-search,.btn-categories, .category-icon").show();
  $(".search-result-section,.remove-all-section, .btn-close").hide();
  $(".main-categories-section").show();
  $("#mainSearchResult, .categories-pagination").html("");
  $("#main-search-text").val("");
});

$(".btn-favorite-close").click(function () {
  $(".btn-advance-search,.btn-categories, .category-icon").show();
  $(".favorite-section, .remove-all-section, .btn-close").hide();
  $(".main-categories-section").show();
  $(".categories-pagination").html("");
});

$(".btn-advance-search-close").click(function () {
  $(".btn-advance-search,.btn-categories, .category-icon").show();
  $(
    ".advance-search-section,.remove-all-section, .btn-close, .asbGoBack"
  ).hide();
  $(".main-categories-section").show();
  $(".categories-pagination").html("");
});

$(".btn-category-close").click(function () {
  $(".btn-advance-search,.btn-categories, .category-icon").show();
  $("#main-categories-data, .categories-pagination, #loadMoreSection").html("");
  $(".categories-section,.remove-all-section, .btn-close, .hvBtns").hide();
});

$(".btn-favorite").click(function () {
  $(".section-common, .main-categories-section, .btn-close").hide();
  $(".favorite-section,.remove-all-section, .btn-favorite-close").show();
  $("#mainSearchResult, .advanceSearchResult").html("");
  /*if ($(this).hasClass('active')) {  
    $(this).removeClass('active');
    $(".btn-favorite, .btn-share, .btn-categories").removeClass('active');
    $(".favorite-section,.remove-all-section").hide(); 
    $(".main-categories-section").show();
  } else { 
    $('.btn-favorite').removeClass('active');
    $(this).addClass('active');
    $(".btn-share, .btn-categories").removeClass('active');
    $(".favorite-section,.remove-all-section").show(); 
  }*/
});

$(".btn-product-search").click(function () {
  doProductSearch();
  /*if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    $(".btn-product-search, .btn-favorite, .btn-share, .btn-categories").removeClass('active');
    $(".search-result-section,.remove-all-section").hide(); 
    $(".main-categories-section").show();
  } else {
    $('.btn-product-search').removeClass('active');
    $(this).addClass('active');
    $(".btn-share, .btn-categories").removeClass('active');
    $(".search-result-section,.remove-all-section").show(); 
  }*/
});

function doProductSearch() {
  let keyword = $("#main-search-text").val();
  keyword = keyword.trim();
  if (!keyword) {
    return false;
  }
  $("#mainSearchResult, .advanceSearchResult").html("");
  searchDesignsByKeyword(keyword);
  $(".section-common, .main-categories-section, .btn-close").hide();
  $(".search-result-section,.remove-all-section, .btn-search-close").show();
}

//Search design result
function searchDesignsByKeyword(keyword, page_number = 1) {
  $("#loadMoreSearch").html(
    `<img src="assets/images/loader.gif" style="width: 40px;">`
  );
  get_designs(
    { search_term: "~" + keyword, _page_size: 20, _page_number: page_number },
    function (result) {
      let {
        is_first,
        total_number,
        page_size,
        page_number,
        is_last,
        data: designs,
      } = result;
      if (is_last) {
        $("#loadMoreSearch").html("");
      }
      if (page_number == 1) {
        $("#mainSearchResult").html("");
        if (!total_number) {
          $("#mainSearchResult").html(
            "<p class='white-color text-center'>No data found!.</p>"
          );
        }
      }
      designs.map((design) => {
        let productKey = design.product_id;
        productKey = productKey.replace(/ /g, "_");
        let singleDesign = `<div class="col-md-6 col-sm-6 col-xs-6">
               <a href="javascript:void(0)" onclick="callChange('${productKey}', '${design.finish}', this)">
                  <div class="mds_viewed_image" data-product_id = '${productKey}' data-finish = '${design.finish}'>
                     <img class="img-responsive size-140x140" src="${design.thumbnail}">
                     <p>${design.product_id} ${design.product_name}</p>
                  </div>
               </a>    
               <a class="downloadbimonactive" id="bimactive_${productKey}" download="${productKey} ${design.product_name}" href="https://general-iamdave.s3.us-west-2.amazonaws.com/octopus_products/bim_file/sketchup/${productKey}.skm"><img style="width: 50px;" src="assets/images/Icons/activefile.png"></a>
            </div>`;
        $("#mainSearchResult").append(singleDesign);
      });
      if (!is_last) {
        let loadMoreBtn = `<div class="col-md-12 text-center"><a href="javascript:void(0)" onclick="searchDesignsByKeyword('${keyword}', ${++page_number})" class="btn btn-load-more">Load More</a></div>`;
        $("#loadMoreSearch").html(loadMoreBtn);
      }
    }
  );
}

//Apply change filter
function callChange(product_id, finish, that) {
  if (that) {
    $(".mds_viewed_image.active").removeClass("active");
    $(that).children("div").addClass("active");
  }

  $(".downloadbimonactive").hide();
  let productKey = product_id;
  productKey = productKey.replace(/ /g, "_");
  $("#bimactive_" + productKey).show();
  // let direction = "vertical";
  // if (localStorage.getItem("direction")) {
  //   direction = localStorage.getItem("direction");
  // }   
  // console.log("direction : ", direction); 
  // change(product_id, finish, null, direction);
 
  getChange(product_id, finish, true);
}

 

$(".bg-block").on("click", 'img', function(){
    const mds_surface_id = $(this).data("mds_surface_id"); 
    localStorage.setItem("selectedSurfaceId", mds_surface_id); 
})
    


$(".setDirection").click(function () {
  const direction = $(this).data("direction");  
  const product_id = $(".mds_viewed_image.active").data("product_id");
  const finish = $(".mds_viewed_image.active").data("finish"); 

  // console.log(`product_id : ${product_id} && finish : ${finish} && direction : ${direction}`)

  // if (direction == "horizontal") {
  //   $(this).data('direction',"vertical"); 
  // } else {
  //   $(this).data('direction',"horizontal"); 
  // }
  // localStorage.setItem("direction", direction); 
  // if(product_id && finish){
  //     //console.log(`product_id : ${product_id} && finish : ${finish} && direction : ${direction}`)
  //     change(product_id, finish, null, direction);
  // }

  getChange(product_id, finish);
});


function getChange(product_id, finish, keep) {
    // START NEW SURFACE TOGGLE LOGIC
    let mds_surface_id = localStorage.getItem("selectedSurfaceId");
    console.log(mds_surface_id)
    console.log("mdsSurfaceInfo : ", mdsSurfaceInfo)

    const mdsSurace = mdsSurfaceInfo.filter((surface)=>{
        return surface.surface_id==mds_surface_id
    })
    let selectedSurface = mdsSurace[0];

    //Get previous direction and toggle it
    let surfaceDirection = selectedSurface.direction;
    if (!keep) {
        surfaceDirection = (selectedSurface.direction=="vertical") ? "horizontal" : "vertical";
    }

    //Remove selected surface
    let filteredSurfaces = mdsSurfaceInfo.filter( el => el.surface_id !== mds_surface_id ); 

    mdsSurfaceInfo = filteredSurfaces;
    console.log("no mdsSurfaceInfo : ", mdsSurfaceInfo);

    //Create new surface with updated direction
    const createSurface = { surface_id : mds_surface_id, direction : surfaceDirection };

    //insert new surface in mdsSurfaceInfo array
    mdsSurfaceInfo.push(createSurface)

    console.log("New mdsSurfaceInfo : ", mdsSurfaceInfo); 
    // END NEW SURFACE TOGGLE LOGIC

    change(product_id, finish, null, surfaceDirection);
}


$(".btn-advance-search").click(function () {
  $(".asb_categories").prop("checked", false);
  $(
    ".section-common, .main-categories-section, .btn-close, .asbGoBack, .asbSubCategoryTitle , .asbSubCategory, #loadMoreAdvanceSearch"
  ).hide();
  $(
    ".btn-advance-search-close, .advance-search-category , .advance-search-section, .remove-all-section"
  ).show();
  $(".advanceSearchResult, .asbSubCategory").html("");

  /*if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(".btn-advance-search, .btn-share, .btn-categories").removeClass('active');
      $(".advance-search-section,.remove-all-section").hide(); 
      $(".main-categories-section").show();
   }else{
      $('.btn-advance-search').removeClass('active');
      $(this).addClass('active');
      $(".btn-share, .btn-categories").removeClass('active');
      $(".advance-search-section,.remove-all-section").show(); 
   }*/
});

$(".btn-categories").click(function () {
  displayCategories();
});

function displayCategories() {
  $(".section-common, .btn-close").hide();
  $(".btn-advance-search").hide();
  $(".categories-section, .remove-all-section, .btn-category-close").show();
  getMainCategories();

  /*if ($('.btn-categories').hasClass('active')) {
      $(".btn-advance-search").show();
      $("#main-categories-data").html(""); 
      $('.btn-categories').removeClass('active');
      $(".btn-favorite, .btn-share, .btn-categories").removeClass('active');
      $(".categories-section,.remove-all-section").hide();
   } else { 
      $('.btn-categories').removeClass('active');
      $('.btn-categories').addClass('active');
      $(".btn-favorite, .btn-share").removeClass('active'); 
      $(".btn-advance-search").hide();
      $(".categories-section, .remove-all-section, .btn-category-close").show();
      getMainCategories();
   }*/
}
