from PIL import Image
import PIL
import os
import sys
for f in sys.stdin:
    f = f.strip()
    print("Processing file: ", f, file = sys.stderr)
    if not ( f.lower().endswith('.png') ):
        print("File " + f + " does not end with png")
        continue
    image = Image.open(f)
    image = image.convert('RGB')
    fo = os.path.splitext(f)[0] + '.jpg'
    image.save(fo, 'jpeg')
    print("Saved file to " + fo)

