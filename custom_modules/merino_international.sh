export BASE_PATH="$( cd "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )" && pwd )"
export ENTERPRISE_ID="merino_international"
export ADMIN_EMAIL="ananth+merinoint@i2ce.in"
export ADMIN_API_KEY="e560a652-8b76-34bb-837c-9509efe07ba9"
export ENTERPRISE_NAME="Merino DigitalFolder International"
export ENTERPRISE_DESCRIPTION="Welcome to the Home of Laminates, welcome to Merino. From a humble beginning with the launch of plywood in 1974 and subsequently high pressure laminates in 1981, the Merino Group has come a long way. Today, the group is one of the world’s largest manufacturers of decorative laminates for interiors and has a global recognition of being a leader in the laminates and panel industry. The Group has diverse business interests that expand from Interior Architectural products to Information Technology to Food & Agro products. With its presence in over 80 countries that span across the five continents, Merino Group achieved an annual turnover of over 215 Million USD for the FY 2018-19."
export ADMIN_PASSWORD="D@veM3rin0I"
export SIGNUP_API_KEY="9368547c-bd11-3c8e-bfa6-accf9d4a0643"
export MODEL_NAMES="product_category product product_design customer wishlist interaction"
export OBJECT_NAMES="interaction_stages.json deployment"

source $BASE_PATH/custom_modules/_function.sh
