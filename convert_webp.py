from PIL import Image
import PIL
import os
import sys
for f in sys.stdin:
    f = f.strip()
    print("Processing file: ", f, file = sys.stderr)
    if not ( f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg') ):
        print("File " + f + " does not end with png, jpg or jpeg")
        continue
    image = Image.open(f)
    image = image.convert('RGBA')
    fo = os.path.splitext(f)[0] + '.webp'
    image.save(fo, 'webp')
    print("Saved file to " + fo)
